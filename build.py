import os
import sys
from pybuilder.core import use_plugin, init, Author, task

name = 'baiko_server'
version = '0.1.2'
url = 'http://stash01:7990/projects/DEV/repos/baiko_server'
description = 'Nex Traiana deployment tool',
authors = [Author('DevOps Dev', 'ildevops@traiana.com')]
license = 'Nex Traiana'
summary = 'Baiko - Traiana deployment tool'
classifiers = ['Production/Stable', 'Programming Language :: Python :: 3.6']

use_plugin('python.install_dependencies')
use_plugin('pypi:pyb_trims')
use_plugin('python.core')
use_plugin("python.flake8")

default_task = ['install_dependencies', 'analyze', 'trims_publish']


@init
def set_properties(project):
    project.set_property('classifiers', [
        'Production/Stable',
        'Programming Language :: Python :: 3.6',
    ])
    project.set_property('manifest', [
        'recursive-include baiko_server/res *',
        'recursive-include baiko_server/config *',
        'recursive-include baiko_server/scripts *'
    ])
    project.set_property('data_files', {'dst_dir_name': '/baiko-docs', 'src_dir': 'docs'})
    project.set_property('scripts', ['scripts/run', 'scripts/generate_swagger.py'])
    project.set_property('dir_source_main_python', 'baiko_server/')
    project.set_property("flake8_break_build", False)
    project.set_property("flake8_include_test_sources", False)
    project.set_property("flake8_include_scripts", False)


@task
def rs(project):
    project_root = os.path.dirname(__file__)
    cmd = 'export PYTHONPATH=%s; pipenv run "gunicorn --chdir ./baiko_server run:api --timeout 500 --bind 127.0.0.1:5000 --workers 10"' % project_root
    print("Gonna execute: %s" % cmd)
    os.system(cmd)


@task
def rc(project):
    project_root = os.path.dirname(__file__)
    cmd = 'export PYTHONPATH=%s; pipenv run "celery -A tasks worker --loglevel=info --workdir ./baiko_server/celery_tasks -n celery@baiko_server" -Q `hostname`,celery' % project_root
    print("Gonna execute: %s" % cmd)
    os.system(cmd)
