#!/usr/bin/env python3.6
import os
import json
import yaml

APP_RUNTIME_TYPE = os.environ['APP_RUNTIME_TYPE']
SWAGGER_ROOT = os.environ['SWAGGER_ROOT']
BAIKO_SERVER_EXTERNAL_URL = os.environ['BAIKO_SERVER_EXTERNAL_URL']


def generate_swagger():
    doc_url = BAIKO_SERVER_EXTERNAL_URL
    schemes = ['http']
    doc_url = doc_url.replace("http://", "")
    if doc_url.find("https://") != -1:
        doc_url = doc_url.replace("https://", "")
        schemes = ['https']

    if APP_RUNTIME_TYPE == 'BAIKO_SERVER':
        api_versions = {
            'v1':
                {
                    'yaml_spec': 'swagger_spec_v1.yaml',
                    'js_spec': 'swagger-ui-spec-v1.js'
                },
            'v2':
                {
                    'yaml_spec': 'swagger_spec_v2.yaml',
                    'js_spec': 'swagger-ui-spec-v2.js'
                }
        }
        print("Gonna generate Swagger docs. . .")
        for version, doc in api_versions.items():
            swagger_spec_location = "{swagger_root}/{yaml_spec}".format(swagger_root=SWAGGER_ROOT, yaml_spec=doc.get('yaml_spec'))
            with open(swagger_spec_location, "r") as f:
                swagger_spec_yaml = f.read()
            f.close()
            json_from_yaml = json.dumps(yaml.load(swagger_spec_yaml), indent=2)
            spec = json.loads(json_from_yaml)
            spec['host'] = doc_url
            spec['schemes'] = schemes
            json_with_host_ip = json.dumps(spec)

            spec_js_content = "var swaggerSpec = " + json_with_host_ip + ";"
            swagger_spec_js = "{swagger_root}/{js_spec}".format(swagger_root=SWAGGER_ROOT, js_spec=doc.get('js_spec'))
            with open(swagger_spec_js, "w") as f:
                f.write(spec_js_content)
            f.close()


generate_swagger()
