from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute
import boto3
import json
import requests
from pyper.globals import IG
from pymongo import MongoClient
import array

# session = boto3.session.Session()
# from baiko_server.common.data_provider import DPDObj
# from sqlalchemy import create_engine
# from sqlalchemy.orm import sessionmaker
# from baiko_server.config import conf
#
# db_client = MongoClient('mongodb://10.152.6.230:27017', connect=False)
# db_client = db_client['baiko_db']
#
# engine = create_engine('postgresql://%s:%s@%s:5432/%s' % (conf.DB_USER, conf.DB_PASS, conf.DB_HOST, conf.DB_NAME))
# DBSession = sessionmaker(bind=engine)
# IG.DBSession = DBSession

# dep = db_client.dobj.find_one({'did': 'ea898fa06b124746bd91d79ec4056761'}, {'_id': False})
# dep['depObj'] = dep['debObj']
# del dep['debObj']
#
# for log in dep['log']:
#     log['time'] = log['time'].strftime("%Y-%m-%d %H:%M:%S")
# # dep['env'] = json.dumps(dep['env'])
# dep['log'] = json.dumps({})
# # dep['dockerProperties'] = json.dumps(dep['dockerProperties'])
# # dep['serviceConfigs'] = json.dumps(dep['serviceConfigs'])
# DPDObj().create_dobj(dep)
# x = 1
#

# for dep in db_client.dobj.find({}, {'_id': False}):
#     try:
#         if 'debObj' not in dep:
#             continue
#         if 'chart_dry_run' not in dep:
#             continue
#         dep['depObj'] = dep['debObj']
#         del dep['debObj']
#         dep['date'] = dep['date'].strftime("%Y-%m-%d %H:%M:%S")
#
#         if not isinstance(dep['chart_dry_run'], (bytes, bytearray)):
#             dep['chart_dry_run'] = dep['chart_dry_run'].encode('utf-8')
#
#         dep['log'] = json.dumps({})
#         DPDObj().create_dobj(dep)
#     except Exception as ex:
#         print(dep['did'])
#         x = 1



# for app in db_client.apps.find():
#     record = {
#         'appName': app['appName'],
#         'appDesc': None,
#         'roles': app['roles']
#     }
#     if app['appDesc']:
#         record['appDesc'] = app['appDesc']
#
#     requests.post("http://127.0.0.1:5000/app", json=record)
#     x = 1



import requests

topics = [
    "netting.eq.feed",
    "netting.eq.classmaste",
    "netting.eq.bucketmaster.buckets",
    "netting.eq.trades",
    "netting.eq.trades.classification",
    "netting.eq.trades.buckets",
    "netting.eq.classification.trades",
    "netting.eq.buckets",
    "netting.eq.buckets.trades",
    "netting.eq.buckets.bucketmaster",
]

kafka_pod = 'zk-kafka-1483921589-8dvtw'
node_ip = '10.150.199.252'
for topic in topics:
    req = 'http://{node_ip}:31201/v1/env/test/pod/{kafka_pod}/exec'.format(node_ip=node_ip, kafka_pod=kafka_pod)
    cmd = "/opt/kafka/bin/kafka-topics.sh --create --zookeeper {node_ip}:2181 --replication-factor 3 --partitions 10 --topic {topic};".format(
        node_ip=node_ip,
        topic=topic
    )
    body = {"cmd": cmd}
    resp = requests.post(req, json=body).json()
    print(resp['data'])