from pick import pick
import json
import subprocess

CONFIGS = {
    'docker17': {
        'BAIKO_SERVER_EXTERNAL_URL': '10.152.172.70:31201',
        'DB_HOST': 'baikolab.c2uwyzkmhbsy.eu-west-1.rds.amazonaws.com',
        'DB_NAME': 'baiko',
        'DB_USER': 'baiko_lab',
        'DB_PASS': 'baiko_master',
        'REDIS_IP': 'localhost',
        'L5D_HOOK_ENABLED': 'enabled',
        'AUTH_UI_REDIRECT': '',
        'OKTA_LOGIN_LINK': '',
        'AUTH_ENABLED': 'disabled'
    },
    'qa_10_150_199_252': {
        'BAIKO_SERVER_EXTERNAL_URL': '10.150.199.252:31202',
        'DB_HOST': 'baiko-pg',
        'DB_NAME': 'baiko',
        'DB_USER': 'baiko',
        'DB_PASS': 'baiko',
        'REDIS_IP': 'localhost',
        'L5D_HOOK_ENABLED': 'disabled',
        'AUTH_UI_REDIRECT': '',
        'OKTA_LOGIN_LINK': '',
        'AUTH_ENABLED': 'disabled'
    },
    'uat-on-prem': {
        'BAIKO_SERVER_EXTERNAL_URL': '10.152.172.70:31201',
        'DB_HOST': 'ny1u-ssi-pg1',
        'DB_NAME': 'baiko',
        'DB_USER': 'baiko',
        'DB_PASS': 'baiko',
        'REDIS_IP': 'localhost',
        'L5D_HOOK_ENABLED': 'disabled',
        'AUTH_UI_REDIRECT': '',
        'OKTA_LOGIN_LINK': '',
        'AUTH_ENABLED': 'disabled'
    },
    'services': {
        'BAIKO_SERVER_EXTERNAL_URL': '10.126.4.5:80',
        'DB_HOST': 'baikoservices.c1qjulyhgoca.eu-west-1.rds.amazonaws.com',
        'DB_NAME': 'baiko',
        'DB_USER': 'baiko_srv',
        'DB_PASS': 'baiko_master',
        'REDIS_IP': 'localhost',
        'L5D_HOOK_ENABLED': 'enabled',
        'AUTH_UI_REDIRECT': 'http://10.126.4.5/login',
        'OKTA_LOGIN_LINK': 'https://nex-internal.okta-emea.com/home/nexgroup_baikoui_1/0oa1jak8zukUC2axT0i7/aln1jatn0hWEHE5AV0i7',
        'AUTH_ENABLED': 'enabled'
    },
    'ci-services': {
        'BAIKO_SERVER_EXTERNAL_URL': '10.126.3.34:31201',
        'DB_HOST': 'baikoservices.c1qjulyhgoca.eu-west-1.rds.amazonaws.com',
        'DB_NAME': 'baiko',
        'DB_USER': 'baiko_srv',
        'DB_PASS': 'baiko_master',
        'REDIS_IP': 'localhost',
        'L5D_HOOK_ENABLED': 'enabled',
        'AUTH_UI_REDIRECT': 'http://10.126.4.5/login',
        'OKTA_LOGIN_LINK': 'https://nex-internal.okta-emea.com/home/nexgroup_baikoui_1/0oa1jak8zukUC2axT0i7/aln1jatn0hWEHE5AV0i7',
        'AUTH_ENABLED': 'enabled'
    },

}


def get_options():
    options = []
    for conf_name, configs in CONFIGS.items():
        options.append({conf_name: configs})
    return options


def get_chart_params(selected_env):
    params = "--set "
    for env_name in selected_env:
        for param_name, param_value in selected_env[env_name].items():
            params += "configs.{param_name}={param_value},".format(param_name=param_name, param_value=param_value)
    params = params[:-1]
    return params


def main():
    # Parse options
    options = get_options()
    title = 'Please choose environment: '
    selected_env, index = pick(options, title)
    print("%s \n %s" % ("SELECTED ENV: ", json.dumps(selected_env, indent=4)))
    # Generate chart params
    chart_deploy_params = get_chart_params(selected_env)
    # Run dry run
    cmd = "/usr/local/bin/helm install --dry-run --debug {chart_deploy_params} --set serverTag=1.2,uiTag=0.2 /Users/dmitrykar/PycharmProjects/tcharts/baiko;".format(chart_deploy_params=chart_deploy_params)
    print("Gonna execute: %s\n" % cmd)
    p = subprocess.Popen(cmd, shell=True)
    p.communicate()


if __name__ == "__main__":
    main()
