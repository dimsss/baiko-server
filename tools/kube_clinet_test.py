import kubernetes.client
from kubernetes import config

import yaml
import time

kubeconfig = "/Users/dmitrykar/PycharmProjects/baiko_server/tools/config"
config.load_kube_config(kubeconfig)


class KubeClient(object):
    NS = "default"

    def __init__(self, conf=None):
        self.conf = conf
        if conf:
            self.core_api = kubernetes.client.CoreV1Api(
                api_client=config.new_client_from_config(context='kube-devops-lab.traiana.lab1')
            )
        else:
            self.core_api = kubernetes.client.CoreV1Api(
                api_client=config.new_client_from_config(context='service-account-context')
            )

    def set_kube_configs(self):
        # f = open('/Users/dmitrykar/.kube/config', 'r')
        # content = f.read()
        # f.close()

        # kubeconfig = yaml.load(content)
        kubeconfig = "/Users/dmitrykar/PycharmProjects/baiko_server/tools/config"
        x = config.load_kube_config(kubeconfig)
        contexts, active_context = config.list_kube_config_contexts(kubeconfig)
        x = 1
        # if self.conf:
        #     kubeconfig = "/Users/dmitrykar/PycharmProjects/baiko_server/tools/config"
        # else:
        #     kubeconfig = "/Users/dmitrykar/PycharmProjects/baiko_server/tools/config2.yaml"

        # f = open('/Users/dmitrykar/.kube/config', 'w+')
        # f.write(yaml.dump(kubeconfig))
        # f.close()

    def fetch_pods(self):
        resp = self.core_api.list_pod_for_all_namespaces()
        print(len(resp.items))


for i in range(0, 100):
    k = KubeClient()
    k.fetch_pods()
    k = KubeClient(True)
    k.fetch_pods()
