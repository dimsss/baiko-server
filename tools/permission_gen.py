from baiko_server.app import resources
from baiko_server.config import conf
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session

'''
SQLs

create table resources(
	resource varchar(2048) primary key
)

create table groups(
	access_group varchar(1024) primary KEY
)

create table users(
	email varchar(1024) primary key,
	access_group varchar(1024),
	preferences json,
	FOREIGN KEY (access_group) REFERENCES groups (access_group)
)

create table acl(
	id SERIAL PRIMARY KEY,
	access_group varchar(1024),
	resource varchar(2048),
	get boolean,
	post boolean,
	put boolean,
	delete boolean,
	FOREIGN KEY (access_group) REFERENCES groups (access_group),
	FOREIGN KEY (resource) REFERENCES resources (resource)
)

'''

'''
Create groups
===================
insert into groups VALUES ('admin')
insert into groups VALUES ('rnd')
insert into groups VALUES ('slack')
insert into groups VALUES ('jopa')
'''


def init_sys_resources():
    for key in dir(resources):
        if isinstance(getattr(resources, key), type):
            if key == 'PyperBase':
                x = getattr(resources, key)
                for res_class in x.__subclasses__():
                    print("insert into resources (resource) values ('{resource}');".format(resource=res_class.__name__))


def init_permissions():
    engine = create_engine('postgresql://%s:%s@%s:5432/%s' % (conf.DB_USER, conf.DB_PASS, conf.DB_HOST, conf.DB_NAME))
    session_factory = sessionmaker(bind=engine)
    Session = scoped_session(session_factory)
    session = Session()
    groups = session.execute("select * from groups").fetchall()
    resources = session.execute("select * from resources").fetchall()
    for group in groups:
        group = group[0]
        for resource in resources:
            resource = resource[0]
            if group == 'admin':
                sql = "insert into acl (access_group, resource,get,post,put,delete) VALUES(:access_group,:resource,TRUE,TRUE,TRUE,TRUE)"
            else:
                sql = "insert into acl (access_group, resource,get,post,put,delete) VALUES(:access_group,:resource,false,false,false,false)"
            session.execute(sql, {'access_group': group, 'resource': resource})
    session.commit()


# init_permissions()
init_sys_resources()
