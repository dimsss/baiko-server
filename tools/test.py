import datetime
import falcon
from pyper.exception import exception_serializer
from pyper.globals import IG, CG
from pyper.middleware import RequestMiddleware, AuthMiddleware
from baiko_server.config import conf
from baiko_server.app import bootstrap
from baiko_server.app.middleware import CORSMiddleware, AccessPermissionMiddleware
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session

engine = create_engine('postgresql://%s:%s@%s:5432/%s' % (conf.DB_USER, conf.DB_PASS, conf.DB_HOST, conf.DB_NAME))
session_factory = sessionmaker(bind=engine)
IG.DBSession = scoped_session(session_factory)

s1 = IG.DBSession()
s2 = IG.DBSession()
print(s1 is s2)
print(s1)
print(s2)
IG.DBSession.remove()
s2 = IG.DBSession()
print("asd %s" % s2)
