from app import resources
from pymongo import MongoClient
from config import conf

db_client = MongoClient('10.126.4.5', connect=False)
db_client = db_client[conf.DB_NAME]


def create_admin_group():
    group = {"accessGroup": "admin", "resources": []}
    for key in dir(resources):
        if key.find("Resource") != -1:
            group['resources'].append(
                {
                    "resourceName": key,
                    "GET": True,
                    "POST": True,
                    "PUT": True,
                    "DELETE": True

                })
    is_group_exists = db_client.accessGroup.find_one({'accessGroup': 'admin'})
    if is_group_exists:
        print("ERROR, admin access group already exists")
    else:
        db_client.accessGroup.insert_one(group)
        print("OK, admin access group successfully created")


def create_read_limited_group():
    group_name = "readLimited"
    group = {"accessGroup": group_name, "resources": []}
    for key in dir(resources):
        if key.find("Resource") != -1:
            group['resources'].append(
                {
                    "resourceName": key,
                    "GET": False,
                    "POST": False,
                    "PUT": False,
                    "DELETE": False

                })
    is_group_exists = db_client.accessGroup.find_one({'accessGroup': group_name})
    if is_group_exists:
        print("ERROR, readLimited access group already exists")
    else:
        db_client.accessGroup.insert_one(group)
        print("OK, readLimited access group successfully created")


def delete_unsupported_apps():
    baiko_apps = [
        "venue-response",
        "sftp-to-s3-synchronizer",
        "jsonprotoconvertor",
        "ngc-messaging-monitoring",
        "npenrichment",
        "notification-router",
        "orgmodel",
        "bulkuploadservice",
        "batching",
        "notification-enricher",
        "frontend-platform"
    ]
    for app in db_client.apps.find():
        if app['appName'] not in baiko_apps:
            db_client.apps.remove({'appName': app['appName']})


delete_unsupported_apps()
#
# create_admin_group()
# create_read_limited_group()
