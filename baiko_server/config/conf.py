import os, json
from configparser import ConfigParser

# Load configs file
config = ConfigParser(os.environ)
app_ini_file = 'dev.app.ini'
PROFILE = os.environ.get('PROFILE')
if PROFILE and os.environ['PROFILE'] == 'prod':
    app_ini_file = 'prod.app.ini'
config.read("{current_dir}/{ini_file}".format(current_dir=os.path.dirname(__file__), ini_file=app_ini_file))

# Application
APP_DOMAIN = config.get('app', 'domain')
APP_PORT = config.get('app', 'port')
APP_NAME = config.get('app', 'app_name')
HELM_BIN = config.get('app', 'helm_bin')
CHART_WORKSPACE = config.get('app', 'chart_workspace')
K8S_CONFIG_DIR = config.get('app', 'k8s_config_dir')
K8S_MERGED_CONF = config.get('app', 'k8s_config_file')
KUBECTL = config.get('app', 'kubectl')

# GIT scanner configs
GIT_AUTH_HEADER = config.get('git_repo_scanner', 'auth_token')
GIT_APP_PROJECTS = config.get('git_repo_scanner', 'projects').split(',')
GIT_API_URL = config.get('git_repo_scanner', 'git_api_url')

# DB
DB_USER = config.get('db', 'user')
DB_PASS = config.get('db', 'pass')
DB_HOST = config.get('db', 'host')
DB_NAME = config.get('db', 'name')
DB_PORT = config.get('db', 'port')

# Redis
REDIS_BACKEND = config.get('redis', 'backend')
REDIS_BROKER = config.get('redis', 'broker')

HELM_REPO_HOST = config.get('helm_repo', 'host')
SSH_USER = config.get('helm_repo', 'ssh_user')
PRIVATE_KEY = config.get('helm_repo', 'private_key')
PACKAGES_DIR = config.get('helm_repo', 'packages_dir')

# Auth
AUTH_ENABLED = (config.get('auth', 'auth_enabled') == 'enabled')
JWT_SECRET = config.get('auth', 'jwt_secret')
ANONYMOUS_TOKEN = config.get('auth', 'anonymous_token')
ACS_URL = config.get('auth', 'acs_url')
ACS_SECURE_URL = config.get('auth', 'acs_secure_url')
UI_REDIRECT = config.get('auth', 'ui_redirect')

# metadata var initialized in bootstrap func, that's called from run.py
SAML_METADATA_UI = None
SAML_METADATA_API = None

# Delete l5d pods after deployment
DELETE_L5D_PODS = (config.get('hooks', 'l5d_hook') == 'enabled')
DEPLOYMENT_HOOKS = (config.get('hooks', 'deployment_hook') == 'enabled')

# Artifactory
ARTIFACTORY_USER = config.get('artifactory', 'artifactory_user')
ARTIFACTORY_PASSWORD = config.get('artifactory', 'artifactory_password')

with open(config.get('artifactory', 'depfile_schema'), 'r') as f:
    DEPFILE_SCHEMA = f.read()
f.close()
DEPFILE_SCHEMA = json.loads(DEPFILE_SCHEMA)
