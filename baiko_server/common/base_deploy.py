import subprocess
from baiko_server.config import conf
from pyper.logger import CLog
from pyper.exception import CException
import falcon
import os
from .utils import dobj_update_n_sync, fetch_dobj_by_did, detect_installation_type, DeploymentType
import base64


class BaseDeploy(object):
    def __init__(self, did):
        self.did = did
        self.dpd = fetch_dobj_by_did(self.did)
        self.dobj = self.fetch_dobj()
        self.chart_workspace = self._create_chart_tmpl_dir()
        self.helm = conf.HELM_BIN
        self.chart_dir = self.get_chart_dir()

    def get_chart_dir(self):
        """
        Get chart dir, in current did is a type of service, chart dir will based on deployServiceName
        if current did is a type of chart, chart dir will be based on chartName
        :return: 
        """
        # Check if current deployment is a type of service
        dtype = detect_installation_type(self.did)
        if dtype == DeploymentType.SERVICE:
            return "%s/%s" % (self.chart_workspace, self.dobj['deployServiceName'])
        # Check if current deployment is a type of chart
        if dtype == DeploymentType.CHART:
            return "%s/%s" % (self.chart_workspace, self.dobj['chartName'])

    def exec_shell_cmd(self, cmd, return_stdout=False):
        self._write_to_log("Gonna execute %s" % cmd)
        if return_stdout:
            p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        else:
            p = subprocess.Popen(cmd, shell=True)
        stdout, stderr = p.communicate()
        if return_stdout:
            if stdout:
                stdout = str(stdout, 'utf-8')
            else:
                stdout = ''
            if stderr:
                stderr = str(stderr, 'utf-8')
            else:
                stderr = ''

            return p.returncode, stdout, stderr
        else:
            return p.returncode, stdout

    def fetch_dobj(self):
        dobj = self.dpd.get_dobj(self.did)
        if not dobj:
            raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "Unable to find deployment record. Provided did: %s" % self.did)
        return dobj

    def _create_chart_tmpl_dir(self):
        """
        Create workspace dir for chart deployment, 
        the location of workspace constructed from conf.CHART_WORKSPACE/DeploymentID
        :return: /path/to/chart/workspace
        """
        workspace_dir = "%s/%s" % (conf.CHART_WORKSPACE, self.did)
        self._write_to_log("The workspace for chart will be in %s" % workspace_dir)
        if not os.path.exists(workspace_dir):
            self._write_to_log("Workspace directory doesn't exists, gonna create it")
            os.makedirs(workspace_dir)
        # Always dump kubeconfig file    
        self._set_kube_configs(workspace_dir)
        return workspace_dir

    def _set_kube_configs(self, workspace):
        kubeconfig = "%s/config" % workspace
        with open(kubeconfig, "w+") as f:
            f.write(str(base64.b64decode(self.dobj['env']['kubeconfig']), 'utf-8'))
        f.close()

    def get_chart_name(self):
        return "{service_name}-{profile}-{tag}.tgz".format(
            service_name=self.dobj['serviceName'],
            profile=self.dobj['env']['envProfile'],
            tag=self.dobj['dockerTag'])

    def get_service_name(self):
        if self.dobj['serviceRole']:
            return "%s-%s" % (self.dobj['serviceName'], self.dobj['serviceRole'])
        else:
            return self.dobj['serviceName']

    def _write_to_log(self, msg, level=None):
        CLog.log(msg, level, None, (self.dpd.append_to_log, self.did))
        # CLog.log(msg, level, None, self.db, 'did', self.did, 'dobj')

    def handel_error(self, msg):
        try:
            dobj_update_n_sync(self.dobj, {'status': 'error', 'errorMessage': msg})
            self._write_to_log(msg, CLog.ERROR)
            raise Exception()
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, ex)
