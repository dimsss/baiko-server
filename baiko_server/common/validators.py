from jsonschema import validate
from pyper.exception import CException
from pyper.logger import CLog
import requests, yaml, falcon
from pyper.globals import IG
from baiko_server.config import conf
from baiko_server.common.data_provider import DPApp, DPEnv
from .api_clients import ArtifactoryClient
import base64


def envs_post_req_schema(req, resp, resource, params):
    schema = {
        "id": "envs_post",
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "additionalProperties": False,
        "required": [
            "kubeconfig",
            "artifactoryBaseUrl",
            "dockerRegistryBaseUrl",
            "dockerRegistries",
            "envName",
            "envProfile",
            "envDescription",
            "sshKey",
            "helmRepo",
            "vault",
            "vaultToken",
            "dnsName"
        ],
        "properties": {
            "kubeconfig": {
                "type": "string"
            },
            "dockerRegistries": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            },
            "artifactoryBaseUrl": {
                "type": "string"
            },
            "dockerRegistryBaseUrl": {
                "type": "string"
            },
            "envName": {
                "type": "string"
            },
            "envProfile": {
                "type": "string",
                "enum": ["lab", "uat", "prod"]
            },
            "envDescription": {
                "type": ["string", "null"]
            },
            "sshKey": {
                "type": ["string", "null"]
            },
            "helmRepo": {
                "type": "array"
            },
            "vault": {
                "type": "string"
            },
            "vaultToken": {
                "type": "string"
            },
            "dnsName": {
                "type": "string"
            }
        }
    }
    try:
        validate(req.context['doc'], schema)
    except Exception as ex:
        raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Request body validation error, %s" % ex.message)


def k8s_config_req_schema(req, resp, resource, params):
    k8s_config_obj = yaml.load(base64.b64decode(req.context['doc']['kubeconfig']).decode('utf-8'))
    schema = {
        "id": "envs_post",
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "additionalProperties": True,
        "required": [
            "apiVersion",
            "clusters",
            "contexts",
            "current-context",
            "kind",
            "users"
        ],
        "properties": {
            "apiVersion": {"type": "string"},
            "clusters": {
                "type": "array",
                "items": {
                    "required": ["name", "cluster"],
                    "properties": {
                        "name": {"type": "string"},
                        "cluster": {
                            "type": "object",
                            "required": ["certificate-authority-data", "server"],
                            "properties": {
                                "certificate-authority-data": {"type": "string"},
                                "server": {"type": "string"}
                            },
                        }
                    },
                }
            },
            "contexts": {
                "type": "array",
                "items": {
                    "required": ["context", "name"],
                    "properties": {
                        "name": {"type": "string"},
                        "context": {
                            "type": "object",
                            "required": ["cluster", "user"],
                            "properties": {
                                "cluster": {"type": "string"},
                                "user": {"type": "string"}
                            }
                        }
                    }
                }
            },
            "current-context": {"type": "string"},
            "kind": {"type": "string", "enum": ["Config"]},
            "users": {
                "type": "array",
                "items": {
                    "required": ["user", "name"],
                    "properties": {
                        "name": {"type": "string"},
                        "user": {
                            "type": "object",
                            "required": ["client-certificate-data", "client-key-data"],
                            "properties": {
                                "client-key-data": {"type": "string"},
                                "client-certificate-data": {"type": "string"}
                            }
                        }
                    }
                }
            }
        }
    }

    try:
        validate(k8s_config_obj, schema)
    except Exception as ex:
        raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Request body validation error, %s" % ex.message)


def validate_env_uniq(payload):
    k8s_config_obj = yaml.load(base64.b64decode(payload['kubeconfig']).decode('utf-8'))
    dp = DPEnv()
    all_envs = dp.get_all_envs()

    current_env = payload['envName']
    context_name = k8s_config_obj['contexts'][0]['name']
    current_context = k8s_config_obj['current-context']
    user = k8s_config_obj['users'][0]['name']
    k8s_properties = [current_context, context_name, user]

    for env in all_envs:
        if env['envName'] == current_env:
            continue

        k8s_config_list = base64.b64decode(env['kubeconfig']).decode('utf-8').split()
        match = next((substr for substr in k8s_properties if substr in k8s_config_list), False)
        if match:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS,
                    'Value `%s` already used in `%s`' % (match, env['envName']))


def charts_post_req_schema(req, resp, resource, params):
    schema = {
        "id": "charts_post",
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "additionalProperties": False,
        "required": [
            "serviceName",
            "dockerRegistry",
            "dockerTag"
        ],
        "properties": {
            "serviceName": {
                "type": "string"
            },
            "dockerRegistry": {
                "type": "string"
            },
            "dockerTag": {
                "type": "string"
            }
        }
    }
    try:
        validate(req.context['doc'], schema)
    except Exception as ex:
        raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Request body validation error, %s" % ex.message)


def installer_post_req_schema(req, resp, resource, params):
    schema = {
        "id": "charts_post",
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "additionalProperties": False,
        "required": [
            "envName",
            "serviceName",
            "dockerRegistry",
            "dockerTag",
            "serviceRole"
        ],
        "properties": {
            "envName": {
                "type": "string"
            },
            "serviceName": {
                "type": "string"
            },
            "dockerRegistry": {
                "type": "string"
            },
            "dockerTag": {
                "type": "string"
            },
            "serviceRole": {
                "type": "string"
            }
        }
    }
    try:
        validate(req.context['doc'], schema)
    except Exception as ex:
        raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Request body validation error, %s" % ex.message)


def check_if_app_exists(req, resp, resource, params):
    if not DPApp().is_app_exists(req.context['doc']['serviceName']):
        raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "Application not found")


def app_put_post_req_schema(req, resp, resource, params):
    schema = {
        "id": "charts_post",
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "additionalProperties": False,
        "required": [
            "appName",
            "appDesc",
            "roles"
        ],
        "properties": {
            "appName": {
                "type": "string"
            },
            "appDesc": {
                "type": ["string", "null"]

            },
            "roles": {
                "items": {
                    "type": "string"
                }
            }
        }
    }
    try:
        validate(req.context['doc'], schema)
    except Exception as ex:
        raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Request body validation error, %s" % ex.message)


def chart_deploy(req, resp, resource, params):
    schema = {
        "id": "charts_post",
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "additionalProperties": False,
        "required": [
            "repo",
            "chart",
            "version"
        ],
        "properties": {
            "repo": {
                "type": "string"
            },
            "chart": {
                "type": "string"

            },
            "version": {
                "type": "string"
            }
        }
    }
    try:
        validate(req.context['doc'], schema)
    except Exception as ex:
        raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Request body validation error, %s" % ex.message)


def validate_depfile(env, docker_registry, service_name, docker_tag):
    env_obj = DPEnv().get_env(env)
    artifactory_url = env_obj['artifactoryBaseUrl'] + "/artifactory"
    ac = ArtifactoryClient(artifactory_url + "/api/", docker_registry, service_name)
    docker_manifest = ac.get_docker_manifest(docker_registry, service_name, docker_tag)
    docker_registry_local = ac.get_registry(docker_registry)
    CLog.log("Starting dep files validation")
    if len(docker_manifest['depFiles']) == 0:
        raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Missing dep files")
    for filename in docker_manifest['depFiles']:
        dep_file_url = "{artifactory_url}/{docker_registry_local}/{service_name}/{docker_tag}/{filename}".format(
            artifactory_url=artifactory_url,
            docker_registry_local=docker_registry_local,
            service_name=service_name,
            docker_tag=docker_tag,
            filename=filename
        )
        try:
            res = requests.get(dep_file_url)
            dep_obj = yaml.safe_load(res.text)
            validate(dep_obj, conf.DEPFILE_SCHEMA)
            CLog.log("%s verified" % dep_file_url)
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Failed to validate %s, %s" % (dep_file_url, ex))
    return True
