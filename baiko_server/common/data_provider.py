from pyper.globals import IG, CG
from pyper.logger import CLog
from .utils import hide_secret_configs
import json
from sqlalchemy import ARRAY, Column, Date, JSON, LargeBinary, String, Text, TEXT, VARCHAR, Integer, ForeignKey, Boolean, text, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.orm import class_mapper, ColumnProperty
from sqlalchemy.ext.declarative import declarative_base
import inflection
from baiko_server.config import conf
import datetime

Base = declarative_base()
metadata = Base.metadata


class BaseMixin(object):
    def as_dict(self):
        result = {}
        for prop in class_mapper(self.__class__).iterate_properties:
            if isinstance(prop, ColumnProperty):
                result[prop.key] = getattr(self, prop.key)
        return result


class App(BaseMixin, Base):
    __tablename__ = 'apps'

    appName = Column('app_name', String(1024), primary_key=True)
    appDesc = Column('app_desc', Text)
    roles = Column(ARRAY(TEXT()))
    updatedBy = Column('updated_by', ARRAY(JSON()))


class Dobj(BaseMixin, Base):
    __tablename__ = 'dobj'

    did = Column(String(1024), primary_key=True)
    errorMessage = Column("error_message", Text)
    date = Column(Date)
    dockerTag = Column("docker_tag", String(1024))
    serviceRole = Column("service_role", String(1024))
    status = Column(String(1024))
    serviceConfigs = Column("service_configs", JSON)
    steps = Column(ARRAY(VARCHAR(length=1024)))
    dockerRegistry = Column("docker_registry", String(1024))
    deployServiceName = Column("deploy_service_name", String(2048))
    env = Column('env', JSON)
    log = Column(ARRAY(JSON()))
    serviceName = Column("service_name", String(1024))
    dockerProperties = Column("docker_properties", JSON)
    depObj = Column("dep_obj", Text)
    chart_dry_run = Column(LargeBinary)
    chartName = Column("chart_name", Text)
    chart = Column(Text)
    updatedBy = Column('updated_by', ARRAY(JSON()))


class ChartDobj(BaseMixin, Base):
    __tablename__ = 'chart_dobj'

    did = Column(String(1024), primary_key=True)
    errorMessage = Column("error_message", Text)
    date = Column(DateTime)
    version = Column(String(1024))
    status = Column(String(1024))
    steps = Column(ARRAY(VARCHAR(length=1024)))
    chartRepo = Column("chart_repo", JSON)
    env = Column(JSON)
    log = Column(ARRAY(JSON()))
    chartName = Column("chart_name", String(1024))
    chartDryRun = Column("chart_dry_run", LargeBinary)
    updatedBy = Column('updated_by', ARRAY(JSON()))


class Env(BaseMixin, Base):
    __tablename__ = 'envs'

    envName = Column("env_name", String(1024), primary_key=True)
    kubeconfig = Column(Text)
    artifactoryBaseUrl = Column("artifactory_base_url", String(1024))
    envProfile = Column("env_profile", String(255))
    envDescription = Column("env_description", Text)
    sshKey = Column("ssh_key", Text)
    vault = Column(String(1024))
    vaultToken = Column("vault_token", String(2014))
    dockerRegistryBaseUrl = Column("docker_registry_base_url", String(1024))
    dockerRegistries = Column("docker_registries", ARRAY(VARCHAR(length=1024)))
    helmRepo = Column('helm_repo', JSON())
    updatedBy = Column('updated_by', ARRAY(JSON()))
    dnsName = Column('dns_name', String(2048))


class Acl(BaseMixin, Base):
    __tablename__ = 'acl'

    id = Column(Integer, primary_key=True, server_default=text("nextval('acl_id_seq'::regclass)"))
    accessGroup = Column("access_group", ForeignKey('groups.access_group'))
    resource = Column(ForeignKey('resources.resource'))
    get = Column(Boolean)
    post = Column(Boolean)
    put = Column(Boolean)
    delete = Column(Boolean)

    group = relationship('Group')
    resource1 = relationship('Resource')


class Group(BaseMixin, Base):
    __tablename__ = 'groups'

    accessGroup = Column("access_group", String(1024), primary_key=True)
    updatedBy = Column('updated_by', ARRAY(JSON()))


class Resource(BaseMixin, Base):
    __tablename__ = 'resources'

    resource = Column(String(2048), primary_key=True)


class User(BaseMixin, Base):
    __tablename__ = 'users'

    email = Column(String(1024), primary_key=True)
    offline_token = Column('offline_token', String(1024))
    accessGroup = Column("access_group", ForeignKey('groups.access_group'))
    preferences = Column(JSON)

    group = relationship('Group')


class BaikoConfigs(BaseMixin, Base):
    __tablename__ = 'baiko_configs'

    saml_metadata = Column('saml_metadata', Text, primary_key=True)
    metadata_type = Column('metadata_type', String(64))


class DataProvider(object):
    def __init__(self, model):
        self.session = IG.DBSession()
        # CLog.log("Data provider session: %s" % self.session)
        self.model = model

    def _get_items(self):
        res = [item.as_dict() for item in self.session.query(self.model).all()]
        self.session.close()
        return res

    def _create_item(self, item_obj):
        """
        Create new item based on item obj
        :param item_obj: 
        :return: None
        """
        self.session.add(self.model(**item_obj))
        self.session.commit()
        self.session.close()

    def _write_audit(self, tbl_name, key_name, key_value):
        updated_by_record = {'email': CG.ui.email, 'date': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
        self.session.execute(
            "update %s set updated_by=array_append(updated_by, :updated_by_record) where %s = :key_value" % (tbl_name, key_name),
            {'updated_by_record': json.dumps(updated_by_record), 'key_value': key_value}
        )
        self.session.commit()
        self.session.close()


class DPBaikoConfigs(DataProvider):
    def __init__(self):
        super().__init__(BaikoConfigs)

    def create_baiko_configs(self, saml_metadata, metadata_type):
        baiko_configs_obj = {
            'saml_metadata': saml_metadata,
            'metadata_type': metadata_type
        }

        self._create_item(baiko_configs_obj)

    def get_baiko_configs_by_type(self, metadata_type):
        baiko_configs = self.session.query(BaikoConfigs).filter(BaikoConfigs.metadata_type == metadata_type.lower()).first()
        self.session.close()
        if baiko_configs.saml_metadata:
            return baiko_configs.saml_metadata
        else:
            return False


class DPApp(DataProvider):
    def __init__(self):
        super().__init__(App)

    def get_apps(self):
        res = [item.as_dict() for item in self.session.query(App).filter().all()]
        self.session.close()
        return res

    def get_app(self, app_name):
        app = self.session.query(App).filter(App.appName == app_name).first()
        self.session.close()
        if app:
            return app.as_dict()
        else:
            return False

    def update_app(self, app_obj):
        self.session.query(App).filter(App.appName == app_obj['appName']).update(app_obj)
        self.session.commit()
        self.session.close()
        if conf.AUTH_ENABLED:
            self._write_audit('apps', 'app_name', app_obj['appName'])

    def is_app_exists(self, app_name):
        app_name = self.get_app(app_name)
        if app_name is False:
            return False
        return True

    def create_app(self, app_obj):
        self._create_item(app_obj)
        if conf.AUTH_ENABLED:
            self._write_audit('apps', 'app_name', app_obj['appName'])

    def delete_app(self, app_name):
        self.session.query(App).filter(App.appName == app_name).delete()
        self.session.commit()
        self.session.close()


class DPEnv(DataProvider):
    def __init__(self):
        super().__init__(Env)

    def get_all_envs(self):
        return self._get_items()

    def get_envs(self, access_group=None):
        if access_group:
            sql = """
            with group_envs AS(
                select allowed_envs from groups where access_group = :access_group
            )
            select * from envs, group_envs where env_name = ANY(allowed_envs)
            """
        else:
            sql = "select * from envs"
        records = self.session.execute(sql, {'access_group': access_group}).fetchall()
        res_data = []
        self.session.close()
        for proxy_row in records:
            row = {}
            for col, data in proxy_row.items():
                row[inflection.camelize(col, False)] = data
            res_data.append(row)
        return res_data

    def create_env(self, env_obj):
        self._create_item(env_obj)
        self.append_env_to_admin_group(env_obj['envName'])
        if conf.AUTH_ENABLED:
            self._write_audit('envs', 'env_name', env_obj['envName'])

    def remove_env_from_admin_group(self, env_name):
        self.session.execute(
            "update groups set allowed_envs=array_remove(allowed_envs, :new_env) where access_group = 'admin'",
            {'new_env': env_name}
        )
        self.session.commit()
        self.session.close()

    def append_env_to_admin_group(self, env_name):
        self.session.execute(
            "update groups set allowed_envs=array_append(allowed_envs, :new_env) where access_group = 'admin'",
            {'new_env': env_name}
        )
        self.session.commit()
        self.session.close()

    def get_env(self, env_name):
        env = self.session.query(Env).filter(Env.envName == env_name).first()
        self.session.close()
        if env:
            return env.as_dict()
        else:
            return False

    def ger_helm_repo_by_name(self, repo_name):
        pass

    def update_env(self, env_obj):
        self.session.query(Env).filter(Env.envName == env_obj['envName']).update(env_obj)
        self.session.commit()
        self.session.close()
        if conf.AUTH_ENABLED:
            self._write_audit('envs', 'env_name', env_obj['envName'])

    def is_env_exists(self, env_name):
        env = self.get_env(env_name)
        if env is False:
            return False
        return True

    def delete_env(self, env_name):
        self.session.query(Env).filter(Env.envName == env_name).delete()
        self.session.commit()
        self.session.close()
        self.remove_env_from_admin_group(env_name)


class DPChartDObj(DataProvider):
    def __init__(self):
        super().__init__(ChartDobj)

    def get_dobj(self, did):
        chart_dobj = self.session.query(ChartDobj).filter(ChartDobj.did == did).first()
        self.session.close()
        if chart_dobj:
            return chart_dobj.as_dict()
        else:
            return False

    def is_dobj_exists(self, did):
        dobj = self.get_dobj(did)
        if dobj is False:
            return False
        return True

    def is_dobj_exists(self, did):
        chart_dobj = self.get_dobj(did)
        if chart_dobj is False:
            return False
        return True

    def create_dobj(self, chart_dobj):
        self._create_item(chart_dobj)
        if conf.AUTH_ENABLED:
            self._write_audit('chart_dobj', 'did', chart_dobj['did'])

    def get_dobjs(self):
        return self._get_items()

    def append_to_log(self, did, log_record):
        self.session.execute(
            "update chart_dobj set log=array_append(log, :log_record) where did = :did",
            {'log_record': json.dumps(log_record), 'did': did}
        )
        self.session.commit()
        self.session.close()

    def append_to_step(self, did, step):
        self.session.execute(
            "update chart_dobj set steps=array_append(steps, :step) where did = :did",
            {'step': step, 'did': did}
        )
        self.session.commit()
        self.session.close()

    def get_deployments(self, env):
        res_data = []
        sql = "select version, did, status, error_message, chart_name, date, updated_by from chart_dobj where env->>'envName' = :envName"
        records = self.session.execute(sql, {'envName': env}).fetchall()
        self.session.close()
        for proxy_row in records:
            row = {}
            for col, data in proxy_row.items():
                if col == 'updated_by':
                    if proxy_row['updated_by'] and len(data) and 'email' in data[0]:
                        row[inflection.camelize(col, False)] = data[0]['email']
                    else:
                        row[inflection.camelize(col, False)] = None
                else:
                    row[inflection.camelize(col, False)] = data
            res_data.append(row)
        return res_data

    def update_by_key(self, did, item_key, item_value):
        if isinstance(item_value, dict):
            item_value = json.dumps(item_value)
        item_key = inflection.underscore(item_key)
        sql = "update chart_dobj set %s=:item_value where did = :did" % item_key
        self.session.execute(sql, {'item_value': item_value, 'did': did})
        self.session.commit()
        self.session.close()


class DPDObj(DataProvider):
    def __init__(self):
        super().__init__(Dobj)

    def get_dobj(self, did, hidden_conf=False):
        dobj = self.session.query(Dobj).filter(Dobj.did == did).first()
        self.session.close()
        if dobj:
            dobj = dobj.as_dict()
            # If configs should be hidden, mutate dobj object
            if hidden_conf:
                dobj['serviceConfigs'] = hide_secret_configs(dobj['serviceConfigs'])
            return dobj
        else:
            return False

    def is_dobj_exists(self, did):
        dobj = self.get_dobj(did)
        if dobj is False:
            return False
        return True

    def create_dobj(self, dobj):
        self._create_item(dobj)
        if conf.AUTH_ENABLED:
            self._write_audit('dobj', 'did', dobj['did'])

    def get_dobjs(self):
        return self._get_items()

    def append_to_log(self, did, log_record):
        self.session.execute(
            "update dobj set log=array_append(log, :log_record) where did = :did",
            {'log_record': json.dumps(log_record), 'did': did}
        )
        self.session.commit()
        self.session.close()

    def append_to_step(self, did, step):
        self.session.execute(
            "update dobj set steps=array_append(steps, :step) where did = :did",
            {'step': step, 'did': did}
        )
        self.session.commit()
        self.session.close()

    def update_by_key(self, did, item_key, item_value):
        if isinstance(item_value, dict):
            item_value = json.dumps(item_value)
        item_key = inflection.underscore(item_key)
        sql = "update dobj set %s=:item_value where did = :did" % item_key
        self.session.execute(sql, {'item_value': item_value, 'did': did})
        self.session.commit()
        self.session.close()

    def get_deployments(self, env):
        res_data = []
        sql = "select docker_tag, did, status, error_message, service_name, date, updated_by from dobj where env->>'envName' = :envName"
        records = self.session.execute(sql, {'envName': env}).fetchall()
        self.session.close()
        for proxy_row in records:
            row = {}
            for col, data in proxy_row.items():
                if col == 'updated_by':
                    if proxy_row['updated_by'] and len(data) and 'email' in data[0]:
                        row[inflection.camelize(col, False)] = data[0]['email']
                    else:
                        row[inflection.camelize(col, False)] = None
                else:
                    row[inflection.camelize(col, False)] = data
            res_data.append(row)
        return res_data

    def get_deployments_stats(self, env):
        """
        Fetch data for chart 
        :return: dict of data for dashboard chart
        """
        chart_data = {'labels': [], 'error': [], 'success': []}
        sql = """select date::timestamp::date as date, array_agg(status) as statuses 
                  from dobj where env->>'envName' = :envName group by date::timestamp::date order by date::timestamp::date """
        for record in self.session.execute(sql, {'envName': env}).fetchall():
            date = str(record[0])
            chart_data['labels'].append(date)
            success = record[1].count('successfully_installed')
            error = len(record[1]) - success
            chart_data['success'].append(success)
            chart_data['error'].append(error)

        self.session.close()
        return chart_data

    def get_top_deployed_services(self, env):
        res = {'labels': [], 'data': []}
        sql = """ select service_name,count(service_name) from dobj where env->>'envName' = :envName group by service_name order by count desc limit 10 """
        for record in self.session.execute(sql, {'envName': env}).fetchall():
            if len(record[0]) > 12:
                res['labels'].append("{}...".format(record[0][:12]))
            else:
                res['labels'].append(record[0])
            res['data'].append(record[1])
        return res


class DPUser(DataProvider):
    def __init__(self):
        super().__init__(User)

    def get_users(self):
        return self._get_items()

    def get_user(self, email):
        user = self.session.query(User).filter(User.email == email).first()
        self.session.close()
        if user:
            return user.as_dict()
        else:
            return False

    def create_user(self, usr_obj):
        self._create_item(usr_obj)

    def is_user_exists(self, email):
        env = self.get_user(email)
        if env is False:
            return False
        return True

    def update_offline_token(self, user_obj, token):
        self.session.execute(
            'update users set offline_token = :offline_token where email = :email',
            {'offline_token': token, 'email': user_obj.get('email')})

        self.session.commit()
        self.session.close()

    def get_identity_data_from_offline_token(self, token):
        query_response = self.session.execute('select * from users where offline_token = :offline_token', {'offline_token': token}).fetchone()
        self.session.close()
        # Each value should be returned as a list because of Okta identity
        return {
            'Email': [query_response[0]],
            'FirstName': [query_response[2]['firstName']],
            'LastName': [query_response[2]['lastName']]
        }


class DPAcl(DataProvider):
    def __init__(self):
        super().__init__(User)

    def get_acl_rule(self, user_email, access_group, resource):
        # This section disabled since as for now, I don't want validate each resource and method for user authorization
        # sql = """
        # with is_user_exists AS (
        #         select (case when count(*) > 0 then TRUE ELSE FALSE END) as user_exists from users where email = :user_email
        # ),
        # rules AS (
        #         select get,post,put,delete,allowed_envs from acl join groups using(access_group)
        #                                     where access_group = :access_group and resource = :resource
        # )
        # select * from is_user_exists,rules
        # """
        sql = """
                with is_user_exists AS (
                        select (case when count(*) > 0 then TRUE ELSE FALSE END) as user_exists from users where email = :user_email
                ), 
                rules AS (
                        select allowed_envs from groups where access_group = :access_group 
                )
                select * from is_user_exists,rules
        """
        rule = self.session.execute(sql, {'access_group': access_group, 'resource': resource, 'user_email': user_email}).fetchone()
        self.session.close()
        return rule

    def get_allowed_envs(self, access_group):
        sql = "select allowed_envs from groups where access_group = :access_group"
        allowed_envs = self.session.execute(sql, {'access_group': access_group}).fetchone()
        self.session.close()
        return allowed_envs
