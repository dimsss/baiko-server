import hvac, paramiko, select, base64, requests, falcon, yaml, datetime, kubernetes.client
from .utils import dobj_update_n_sync, detect_installation_type, DeploymentType, hide_secret_configs, \
    patch_hidden_values_in_configs
from .data_provider import DPDObj, DPEnv
from .base_deploy import BaseDeploy
from kubernetes import config, watch
from kubernetes.client import Configuration
from kubernetes.client.rest import ApiException
from kubernetes.stream import stream
from baiko_server.config import conf
from baiko_server.common.utils import calculate_grpc_gw_ports
from pyper.logger import CLog
from pyper.exception import CException
from pyper.globals import CG
from collections import Iterable
from kubernetes.stream import stream
import threading


def transform_secret_object_to_key_value(data_dict):
    data = data_dict.copy()
    for key, value in data.items():
        if isinstance(value, dict) and 'isHidden' in data.get(key).keys():
            data.update({key: data.get(key).get('value')})
    return data


class BaseClient(object):
    def __init__(self, base_url):
        self.url = base_url

    def _exec_request(self, uri, method='GET', http_basic_auth=()):
        url = "%s%s" % (self.url, uri)
        CLog.log("Gonna execute %s to %s" % (method, url))

        if method == 'GET':
            if http_basic_auth:
                res = requests.get(url, auth=http_basic_auth)
            else:
                res = requests.get(url)
        try:
            return res.json()
        except Exception as ex:
            raise ex

    def get_http_basic_auth(self):
        return False


class ArtifactoryClient(BaseClient):
    def __init__(self, artifactory_url, registry_name, image_name=None):
        super(ArtifactoryClient, self).__init__(artifactory_url)
        CLog.log("Initiated Artifactory client")
        self.registry = self.get_registry(registry_name)
        self.image_name = image_name

    def _artifactory_warmup_because_tomer_doenst_sync(self, uri):
        full_url = "%s%s" % (self.url, uri)
        full_url = full_url.replace('/api/storage', '').replace('?properties', '')
        CLog.log("Warmup GET request: %s" % full_url)
        requests.get(full_url, auth=(conf.ARTIFACTORY_USER, conf.ARTIFACTORY_PASSWORD))

    def get_registry(self, full_registry_name):
        """
        :param full_registry_name: example  'docker-dev-local.artifactory.traiana.com', the registry will be docker-dev-local
        :return:  docker-dev-local
        """
        return full_registry_name.split(".")[0]

    def get_docker_repos(self):
        uri = '/docker/%s/v2/_catalog' % self.registry
        return self._exec_request(uri, 'GET', (conf.ARTIFACTORY_USER, conf.ARTIFACTORY_PASSWORD))

    def get_image_tags(self):
        uri = '/docker/%s/v2/%s/tags/list' % (self.registry, self.image_name)
        return self._exec_request(uri, 'GET', (conf.ARTIFACTORY_USER, conf.ARTIFACTORY_PASSWORD))

    def get_docker_manifest(self, registry, repository, tag):
        uri = "/storage/{registry}/{repository}/{tag}/manifest.json?properties".format(
                registry=self.get_registry(registry), repository=repository, tag=tag)
        self._artifactory_warmup_because_tomer_doenst_sync(uri)
        resp = self._exec_request(uri, 'GET', (conf.ARTIFACTORY_USER, conf.ARTIFACTORY_PASSWORD))
        if 'properties' in resp:
            image_props = {}
            for prop in resp['properties']:
                if len(resp['properties'][prop]):
                    if len(resp['properties'][prop]) == 1 and prop != 'depFiles':
                        image_props[prop.replace(".", "_")] = resp['properties'][prop][0]
                    else:
                        image_props[prop.replace(".", "_")] = resp['properties'][prop]
                else:
                    return None
            return image_props
        return None


class VaultClient(object):
    def __init__(self, env):
        self.env = env
        dp = DPEnv()
        env_metadata = dp.get_env(self.env)
        try:
            self.client = hvac.Client(url=env_metadata['vault'], token=env_metadata['vaultToken'])
            self.base_path = 'secret'
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Unable connect to Vault %s" % ex)

    def fetch_service_configs(self, service, role=None, as_base64=False, secure=False):
        try:
            if role and role != 'none':
                vault_url = "%s/%s/%s/%s" % (self.base_path, self.env, service, role)
            else:
                vault_url = "%s/%s/%s" % (self.base_path, self.env, service)
            res = self.client.read(vault_url)
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Unable read from Vault %s" % ex)

        if res and 'data' in res:
            if secure:
                res['data']['value'] = hide_secret_configs(res['data']['value'])

            if as_base64:
                res['data']['value'] = transform_secret_object_to_key_value(res['data']['value'])
                res_base64 = {}
                for key, value in res['data']['value'].items():
                    # TODO: WTF with casting here ?! I need to understated how to handle the encode/decode
                    if not isinstance(value, str):
                        value = str(value)
                    res_base64[key] = base64.b64encode(value.encode()).decode("utf-8")
                return res_base64
            return res['data']['value']
        else:
            return {}

    def fetch_service_annotations(self, service, role=None):
        try:
            if role and role != 'none':
                vault_url = "%s/%s/annotations-k8s/%s/%s" % (self.base_path, self.env, service, role)
            else:
                vault_url = "%s/%s/annotations-k8s/%s" % (self.base_path, self.env, service)
            res = self.client.read(vault_url)
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Unable read from Vault %s" % ex)
        if res and 'data' in res:
            return res['data']['value']
        else:
            return {}

    def push_service_annotation(self, service, data, role=None):
        try:
            if role and role != 'none':
                vault_url = "%s/%s/annotations-k8s/%s/%s" % (self.base_path, self.env, service, role)
            else:
                vault_url = "%s/%s/annotations-k8s/%s" % (self.base_path, self.env, service)
            self._backup_vault(vault_url)
            self.client.write(vault_url, None, value=data, author=CG.ui.email)
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Unable write to Vault %s" % ex)

    def push_configs(self, service, data, role=None):
        try:
            if role and role != 'none':
                vault_url = "%s/%s/%s/%s" % (self.base_path, self.env, service, role)
            else:
                vault_url = "%s/%s/%s" % (self.base_path, self.env, service)
            patch_hidden_values_in_configs(data, self.fetch_service_configs(service, role))
            self._backup_vault(vault_url)
            self.client.write(vault_url, None, value=data, author=CG.ui.email)
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Unable write to Vault %s" % ex)


    def _backup_vault(self, vault_url):
        previous = self.client.read(vault_url)
        if not previous:
            return
        if 'author' not in previous['data']:
            previous['data']['author'] = 'anonymous@anonymous.com'
        if 'value' in previous['data']:
            now = str(datetime.datetime.now())
            self.client.write(f"{vault_url}-audit/{now}", value=previous['data']['value'], author=previous['data']['author'])


class SSHClient:
    def __init__(self, host):
        self.username = conf.SSH_USER
        self.key = self._set_key(conf.PRIVATE_KEY)
        self.host = host
        self.ssh_client = self._set_ssh_client(host)
        self._sftp = None

    def _set_ssh_client(self, host):
        try:
            ssh_client = paramiko.SSHClient()
            ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            CLog.log("Trying to connect to: {} by key".format(self.host))
            ssh_client.connect(host, username=self.username, pkey=self.key, timeout=20)
            return ssh_client
        except Exception as ex:
            raise ex

    def get_sftp_connections(self):
        if self._sftp is None:
            self._sftp = self.ssh_client.open_sftp()
            return self._sftp
        return self._sftp

    def _set_key(self, key_file_path):
        try:
            return paramiko.RSAKey.from_private_key_file(key_file_path)
        except Exception as ex:
            raise ex

    def exec_cmd_as_root(self, cmd):
        cmd = "sudo -- sh -c '%s'" % cmd
        self.exec_cmd(cmd)

    def exec_cmd(self, cmd):
        try:
            CLog.log('Gonna execute: %s on %s' % (cmd, self.host))
            stdin, stdout, stderr = self.ssh_client.exec_command(cmd)
            # TODO: could be a problem here, how the read() is works? what is a max size that could be read?
            ret_status = stdout.channel.recv_exit_status()
            # Make sure the command returned success code
            if int(ret_status) is not 0:
                CLog.log(
                        'Non zero code: %s returned from the command execution: %s on :%s' % (
                            ret_status, cmd, self.host))
                raise Exception("Non zero code returned from the command execution, can't continue installation")
            if stdout:
                output = stdout.read()
                if output:
                    CLog.log('Fetched:\n')
                    for line in output.split("\n"):
                        CLog.log('%s' % line)
                return output
            return ''
        except Exception as ex:
            raise ex

    def exec_sync_cmd(self, cmd):
        CLog.log('Gonna execute: %s on %s' % (cmd, self.host))
        channel = self.ssh_client.get_transport().open_session()
        channel.get_pty()
        channel.exec_command(cmd)
        while True:
            if channel.exit_status_ready():
                break
            r, w, x = select.select([channel], [], [], 0.0)
            if len(r) > 0:
                yield channel.recv(1024)
        ret_status = channel.recv_exit_status()
        CLog.log('RETURN STATUS: %s' % ret_status)
        channel.close()
        # Make sure the command returned success code
        if int(ret_status) is not 0:
            CLog.log('Non zero code: %s returned from the command execution: %s on: %s' % (ret_status, cmd, self.host))
            raise Exception("Non zero code returned from the command execution, can't continue installation")

    def get_remote_file(self, file_path):
        CLog.log('Gonna bring file: %s from %s' % (file_path, self.host))
        sftp = self.get_sftp_connections()
        f = sftp.open(file_path, mode='r')
        file_content = f.read()
        f.close()
        return file_content

    def put_remote_file(self, file_name, content):
        CLog.log('Gonna put file: %s to %s' % (file_name, self.host))
        sftp = self.get_sftp_connections()
        f = sftp.open(file_name, mode='w')
        f.write(content)
        f.close()


class KubeClient(object):
    NS = "default"

    def __init__(self, env):
        self.env = env
        self.env_obj = self.fetch_env_obj()
        # Set api client
        self.set_kube_client()
        self.core_api = kubernetes.client.CoreV1Api()
        self.batch_api = kubernetes.client.BatchV1Api()
        self.beta_api = kubernetes.client.AppsV1beta1Api()

    def fetch_env_obj(self):
        env_obj = DPEnv().get_env(self.env)
        if not env_obj:
            raise CException(falcon.HTTP_404, CException.ERROR_STATUS,
                             "Unable to find environment record. Provided env: %s" % self.env)
        return env_obj

    def set_kube_client(self):
        try:
            # Configure kube client
            c = Configuration()
            c.assert_hostname = False
            Configuration.set_default(c)
            # Get env context
            env_kubeconfig = yaml.load(str(base64.b64decode(self.env_obj['kubeconfig']), 'utf-8'))
            context = env_kubeconfig['contexts'][0]['name']
            config_file = "%s/%s" % (conf.K8S_CONFIG_DIR, conf.K8S_MERGED_CONF)
            config.load_kube_config(config_file=config_file, context=context)
        except Exception as ex:
            err_msg = "Unable to fetch context name from kubeconfig file for env %s. %s" % (self.env_obj['envName'], ex)
            CLog.log(err_msg, CLog.ERROR)
            raise CException(err_msg, falcon.HTTP_400, CException.ERROR_STATUS)

    def get_all_configmaps(self):
        CLog.log("Fetching all Configmaps")
        api_response = self.core_api.list_config_map_for_all_namespaces()
        configmaps = []
        for configmap in api_response.items:
            CLog.log("Configmap: %s" % configmap.metadata.name)
            configmaps.append(configmap.metadata.name)
        return configmaps

    def is_configmap_exists(self, configmap_name):
        if configmap_name in self.get_all_configmaps():
            return True
        return False

    def is_secret_exists(self, secret_name):
        if secret_name in self.get_all_secrets():
            return True
        return False

    def get_all_secrets(self):
        resp = self.core_api.list_namespaced_secret(KubeClient.NS)
        secrets = []
        CLog.log("*** Available secrets lists ***")
        for secret in resp.items:
            CLog.log("Secret: %s" % secret.metadata.name)
            secrets.append(secret.metadata.name)
        CLog.log("*******************************")
        return secrets

    # def delete_job(self, job_name):
    # 	body = kubernetes.client.V1DeleteOptions()
    # 	try:
    # 		resp = self.batch_api.delete_namespaced_job(job_name, KubeClient.NS, body)
    # 		CLog.log("Job %s successfully deleted" % job_name)
    # 	except Exception as ex:
    # 		CLog.log(ex)

    # raise ex

    def delete_service_secrets(self, secret_name):
        CLog.log("Gonna delete services secrets")
        try:
            body = kubernetes.client.V1Secret()
            CLog.log("Deleting secret: %s" % secret_name)
            self.core_api.delete_namespaced_secret(secret_name, KubeClient.NS, body)
            CLog.log("Deleting secret: %s-file" % secret_name)
            self.core_api.delete_namespaced_secret("%s-file" % secret_name, KubeClient.NS, body)
        except Exception as e:
            CLog.log("Failed to delete secrets for service name: {0}, error: {1}".format(secret_name, e), CLog.ERROR)

    def fetch_replication_controller(self, did):
        try:
            label_selector = "%s=%s" % ('did', did)
            resp = self.beta_api.list_namespaced_deployment(KubeClient.NS, label_selector=label_selector, pretty=True)
            if len(resp.itmes):
                return resp.itmes
            else:
                raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "Deployment not found")

        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS,
                             "Unable to fetch replication controllers. Ex: %s" % ex)

    def connect_pod_exec(self, env, namespace, pod_name, cmd, container=None):
        try:
            self.core_api.read_namespaced_pod(pod_name, namespace)
        except ApiException as e:
            raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "Pod %s not found, %s" % (pod_name, e))
        exec_string = "/bin/sh -c " + cmd
        exec_command = exec_string.split(" ", 2)
        try:
            if container:
                resp = stream(
                        self.core_api.connect_get_namespaced_pod_exec, pod_name, namespace,
                        command=exec_command, container=container,
                        stderr=True, stdin=False,
                        stdout=True, tty=False)
            else:
                resp = stream(
                        self.core_api.connect_get_namespaced_pod_exec, pod_name, namespace,
                        command=exec_command,
                        stderr=True, stdin=False,
                        stdout=True, tty=False)
            return resp
        except ApiException as e:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS,
                             "Unable to exec %s on pod %s, error %s" % (exec_command, pod_name, e))

    def fetch_app_configs(self, dobj, as_base64=True):
        service = dobj['serviceName']
        service_role = dobj['serviceRole']
        app_configs = VaultClient(dobj['env']['envName']).fetch_service_configs(service, service_role, as_base64)

        if as_base64:
            env_profile = base64.b64encode(dobj['env']['envProfile'].encode()).decode('utf-8')
        else:
            env_profile = dobj['env']['envProfile']

        if not app_configs:
            self._write_to_log("No config for %s %s. Empty config will be used" % (service, service_role), dobj["did"],
                               CLog.WARNING)
            app_configs = {'PROFILE': env_profile}
        elif 'PROFILE' not in app_configs:
            app_configs['PROFILE'] = env_profile

        return app_configs

    def generate_k8s_secret_as_env_vars(self, dobj):
        deploy_service_name = dobj['deployServiceName']
        service = dobj['serviceName']
        service_role = dobj['serviceRole']
        self._write_to_log("Gonna create Secret for service: %s role: %s as env var" % (service, service_role),
                           dobj['did'])
        app_configs = self.fetch_app_configs(dobj)
        body = kubernetes.client.V1Secret()
        body.api_version = "v1"
        body.kind = "Secret"
        body.metadata = {"name": deploy_service_name, "namespace": "default", "labels": {'did': dobj['did']}}
        body.data = app_configs
        self._try_and_create_secret(deploy_service_name, dobj, body)

    def generate_k8s_secret_as_file(self, dobj):
        deploy_service_name = "%s-file" % dobj['deployServiceName']
        service = dobj['serviceName']
        service_role = dobj['serviceRole']
        body = kubernetes.client.V1Secret()
        self._write_to_log("Gonna create Secret for service: %s role: %s as file" % (service, service_role),
                           dobj['did'])
        app_configs = self.fetch_app_configs(dobj, as_base64=False)
        body.metadata = {"name": deploy_service_name, "namespace": "default", "labels": {'did': dobj['did']}}
        configs_str = ''
        app_configs = transform_secret_object_to_key_value(app_configs)
        for key, value in app_configs.items():
            try:
                int_val = int(value)
                configs_str += '{}={}\n'.format(key, int_val)
            except ValueError:
                configs_str += '{}="{}"\n'.format(key, value)
            except TypeError as ex:
                configs_str += '{}="{}"\n'.format(key, value)
                CLog.log("Unable to cast, gonna use string %s=%s" % (key, value), CLog.WARNING)
            except Exception as ex:
                self._write_to_log("Unable to generate file based Secret. Ex: %s " % str(ex), dobj['did'], CLog.ERROR)
                raise CException(falcon.HTTP_400, 'Unable to generate file based Secret')

        configs64 = base64.b64encode(bytes(configs_str, 'utf-8'))
        body.data = {'conf': configs64.decode('utf-8')}
        self._try_and_create_secret(deploy_service_name, dobj, body)

    def _try_and_create_secret(self, deploy_service_name, dobj, body):
        try:
            if self.is_secret_exists(deploy_service_name):
                self._write_to_log("Gonna update existing secret %s" % deploy_service_name, dobj['did'], CLog.WARNING)
                resp = self.core_api.patch_namespaced_secret(deploy_service_name, KubeClient.NS, body)
                self._write_to_log("Secret %s successfully updated" % deploy_service_name, dobj['did'])
            else:
                resp = self.core_api.create_namespaced_secret(KubeClient.NS, body)
                self._write_to_log("Secret %s successfully created" % deploy_service_name, dobj['did'], CLog.SUCCESS)
        except Exception as ex:
            self._write_to_log("Unable create secret for service, check Celery logs", dobj['did'], CLog.ERROR)
            dobj_update_n_sync(dobj, {'status': 'error', 'errorMessage': 'Unable create secret for service'})
            CLog.log(ex)
            raise ex

    def generate_k8s_configmap(self, dobj):
        deploy_service_name = dobj['deployServiceName']
        service = dobj['serviceName']
        service_role = dobj['serviceRole']
        CLog.log("Gonna create ConfigMap for service: %s role: %s " % (service, service_role))
        configmap_data = VaultClient(dobj['env']['envName']).fetch_service_configs(service, service_role)
        body = kubernetes.client.V1ConfigMap()
        body.api_version = "v1"
        body.kind = "ConfigMap"
        body.metadata = {"name": deploy_service_name, "namespace": "default"}
        body.data = configmap_data
        try:
            if self.is_configmap_exists(deploy_service_name):
                CLog.log("Gonna update existing configs map %s" % deploy_service_name, CLog.WARNING)
                resp = self.core_api.patch_namespaced_config_map(deploy_service_name, KubeClient.NS, body)
                CLog.log("ConfigMap successfully updated")
            else:
                resp = self.core_api.create_namespaced_config_map(KubeClient.NS, body)
                CLog.log("ConfigMap successfully created")
        except Exception as ex:
            self._write_to_log("Unable create secret for service, check Celery logs", CLog.ERROR)
            dobj_update_n_sync(dobj, {'status': 'error', 'errorMessage': 'Unable create secret for service'})
            CLog.log(ex)
            raise ex

    def get_k8s_nodes(self):
        api_instance = kubernetes.client.CoreV1Api()
        api_response = api_instance.list_node()
        print(api_response)

    def fetch_pod_logs(self, pod_name):
        try:
            resp = self.core_api.read_namespaced_pod_log(pod_name, KubeClient.NS, tail_lines=2000)
            return resp
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Unable to fetch pod logs")

    def _write_to_log(self, msg, did, level=None):
        CLog.log(msg, level, None, (DPDObj().append_to_log, did))

    def fetch_pods(self, label_selector: tuple = None):
        """
        Fetch pods in all namespaces, if label selector provided search by label
        :param label_selector: tuple (label_name,label_value)
        :return: list of pods
        """
        try:
            pods = []
            if label_selector:
                label_selector = "%s=%s" % (label_selector[0], label_selector[1])
                CLog.log("Gonna fetch K8S by selector %s " % label_selector)
                resp = self.core_api.list_pod_for_all_namespaces(label_selector=label_selector)
            else:
                CLog.log("Gonna fetch ALL pods in default namespace")
                resp = self.core_api.list_pod_for_all_namespaces()
            for pod in resp.items:
                if pod.metadata.namespace == 'default':
                    pods.append(self._item_to_pod_details(pod))
            if pods:
                return pods
            else:
                raise CException(falcon.HTTP_404, CException.ERROR_STATUS,
                                 "Didn't found any pods, the pods list is empty")
        except CException as ex:
            raise ex
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Unable to fetch pods. Ex: %s" % ex)

    def fetch_pod(self, pod_name):
        try:
            resp = self.core_api.read_namespaced_pod(pod_name, KubeClient.NS)
            return self._item_to_pod_details(resp)

        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Unable to fetch pods. Ex: %s" % ex)

    def _item_to_pod_details(self, pod):
        try:
            pod_details = {}
            # Default values fro app name and did
            pod_details['app_name'] = "Unknown"
            pod_details['did'] = '-'
            pod_details['issuer'] = 'Unknown'
            pod_details['tsver'] = 'Unknown'

            # If labels are set and they are iterable, check if app and did exists
            if isinstance(pod.metadata.labels, Iterable):
                if 'app' in pod.metadata.labels:
                    pod_details['app_name'] = pod.metadata.labels['app']
                if 'did' in pod.metadata.labels:
                    pod_details['did'] = pod.metadata.labels['did']
                if 'issuer' in pod.metadata.labels:
                    pod_details['issuer'] = pod.metadata.labels['issuer']
                if 'tsver' in pod.metadata.labels:
                    pod_details['tsver'] = pod.metadata.labels['tsver']

            pod_details['pod_name'] = pod.metadata.name
            pod_details['created_at'] = pod.metadata.creation_timestamp
            pod_details['self_link'] = pod.metadata.self_link
            pod_details['host_ip'] = pod.status.host_ip
            pod_details['phase'] = pod.status.phase
            pod_details['pod_ip'] = pod.status.pod_ip
            pod_details['container_name'] = 'Unknown'
            pod_details['containers'] = []
            if pod.status.container_statuses:
                for container in pod.status.container_statuses:
                    container_details = {
                        'name':          container.name,
                        'image':         container.image,
                        'restart_count': container.restart_count,
                        'started_at':    False,
                        'up_time':       False
                    }
                    container_details['state'] = 'Unknown'
                    pod_details['container_name'] = container.name
                    if container.state.waiting:
                        container_details['state'] = container.state.waiting.reason
                    if container.state.running:
                        container_details['state'] = "Running"
                        container_details['started_at'] = container.state.running.started_at.replace(microsecond=0)
                        now = datetime.datetime.now(container_details['started_at'].tzinfo).replace(microsecond=0)
                        container_details['up_time'] = str(now - container_details['started_at'])
                    pod_details['containers'].append(container_details)
            return pod_details
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Failed to pars item to pod details %s" % ex)

    def _itme_to_node_details(self, node_item):
        return {
            'annotations':               node_item.metadata.annotations,
            'labels':                    node_item.metadata.labels,
            'name':                      node_item.metadata.name,
            'self_link':                 node_item.metadata.self_link,
            'pod_cidr':                  node_item.spec.pod_cidr,
            'provider_id':               node_item.spec.provider_id,
            'external_id':               node_item.spec.external_id,
            'addresses':                 list(
                    map(lambda addressObj: {'address': addressObj.address, 'type': addressObj.type},
                        node_item.status.addresses)),
            'allocatable':               node_item.status.allocatable,
            'capacity':                  node_item.status.capacity,
            'images':                    list(
                    map(lambda imageObj: {'names': imageObj.names, 'siz_bytes': imageObj.size_bytes},
                        node_item.status.images)),
            'container_runtime_version': node_item.status.node_info.container_runtime_version,
            'kernel_version':            node_item.status.node_info.kernel_version,
            'kube_proxy_version':        node_item.status.node_info.kube_proxy_version,
            'kubelet_version':           node_item.status.node_info.kubelet_version,
            'operating_system':          node_item.status.node_info.operating_system,
            'os_image':                  node_item.status.node_info.os_image,
        }

    def _item_to_component(self, item):
        return {
            'name':       item.metadata.name,
            'conditions': list(map(
                    lambda conditionObj: {
                        'error':  conditionObj.error, 'message': conditionObj.message,
                        'status': conditionObj.status
                    }, item.conditions))
        }

    def fetch_service(self, did):
        try:
            label_selector = "%s=%s" % ("did", did)
            resp = self.core_api.list_namespaced_service(KubeClient.NS, label_selector=label_selector)
            services = []
            if not len(resp.items):
                raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "Not found any service for did " % did)
            for service in resp.items:
                ports = []
                for port in service.spec.ports:
                    ports.append({
                        'name':      port.name,
                        'node_port': port.node_port,
                        'port':      port.port,
                        'protocol':  port.protocol
                    })
                    services.append(
                            {'name': service.metadata.name, 'ports': ports, 'dnsName': self.env_obj.get('dnsName')})
            return services
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS,
                             "Unable to fetch service controllers. Ex: %s" % ex)

    def get_nodes_list(self):
        try:
            nodes = []
            resp = self.core_api.list_node()
            for item in resp.items:
                nodes.append(self._itme_to_node_details(item))
            return nodes
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Unable to get cluster nodes. Ex: %s" % ex)

    def list_components_status(self):
        try:
            components = []
            resp = self.core_api.list_component_status()
            for item in resp.items:
                components.append(self._item_to_component(item))
            return components
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Unable to get components status. Ex: %s" % ex)

            return nodes
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Unable to get components status. Ex: %s" % ex)

    def delete_job(self, job_name: str):
        try:
            CLog.log(f"Gonna delete job {job_name}")
            del_pol = kubernetes.client.V1DeleteOptions(propagation_policy="Background")
            self.batch_api.delete_namespaced_job(job_name, KubeClient.NS, del_pol)
        except Exception as ex:
            CLog.log(f"Unable delete JOB {job_name}, ex:{ex}", CLog.WARNING)


class BK8SHooks(KubeClient):
	HOOK_SUCCESS_RESULT = 'success'
	HOOK_ERROR_RESULT = 'error'
	HOOK_UNKNOWN_RESULT = 'unknown'

	def __init__(self, env):
		super().__init__(env)
		self._is_success_done = BK8SHooks.HOOK_UNKNOWN_RESULT

	def set_hook_exec_result(self, result: str):
		self._is_success_done = result

	def get_hook_exec_result(self):
		return self._is_success_done

	def _sanitize_env_var_name(self, env_var_name):
		return str(env_var_name).replace(".", "_")

	def _sanitize_env_var_value(self, env_var_value):
		return str(env_var_value)

	def _get_grpc_ports(self, dobj):
		dep_obj = yaml.load(dobj['depObj'])
		calculate_grpc_gw_ports(dep_obj)
		return dep_obj['resources']['grpcGateway']

	def _get_default_env_vars(self, dobj):
		grpc_gw_ports = self._get_grpc_ports(dobj)
		NODE_NAME = kubernetes.client.V1EnvVarSource(
				field_ref=kubernetes.client.V1ObjectFieldSelector(field_path="spec.nodeName"))
		return [
			kubernetes.client.V1EnvVar(
					"APP_NAME",
					self._sanitize_env_var_value(dobj['deployServiceName'])),
			kubernetes.client.V1EnvVar(
					"PROFILE",
					self._sanitize_env_var_value(dobj['env']['envProfile'])),
			kubernetes.client.V1EnvVar(
					"CONTAINER_NAME",
					self._sanitize_env_var_value(dobj['deployServiceName'])),
			kubernetes.client.V1EnvVar(
					"APPLICATION_NAME",
					self._sanitize_env_var_value(dobj['deployServiceName'])),
			kubernetes.client.V1EnvVar(
					"K8S_NAME",
					self._sanitize_env_var_value(dobj['env']['envName'])),
			kubernetes.client.V1EnvVar(
					"GW_INC_HTTP",
					self._sanitize_env_var_value(grpc_gw_ports['GW_INC_HTTP'])),
			kubernetes.client.V1EnvVar(
					"GW_INC_HTTP_OPS",
					self._sanitize_env_var_value(grpc_gw_ports['GW_INC_HTTP_OPS'])),
			kubernetes.client.V1EnvVar(
					"GW_OUT_GRPC",
					self._sanitize_env_var_value(grpc_gw_ports['GW_OUT_GRPC'])),
			kubernetes.client.V1EnvVar(
					"GW_OUT_GRPC_OPS",
					self._sanitize_env_var_value(grpc_gw_ports['GW_OUT_GRPC_OPS'])),
			kubernetes.client.V1EnvVar("NODE_NAME", value_from=NODE_NAME),
			kubernetes.client.V1EnvVar("POD_NAME", value_from=kubernetes.client.V1EnvVarSource(
					field_ref=kubernetes.client.V1ObjectFieldSelector(field_path="metadata.name"))),
			kubernetes.client.V1EnvVar("POD_IP", value_from=kubernetes.client.V1EnvVarSource(
					field_ref=kubernetes.client.V1ObjectFieldSelector(field_path="status.podIP"))),
			kubernetes.client.V1EnvVar("SERVICE_ACCOUNT", value_from=kubernetes.client.V1EnvVarSource(
					field_ref=kubernetes.client.V1ObjectFieldSelector(field_path="spec.serviceAccountName"))),
			kubernetes.client.V1EnvVar("POD_NAMESPACE", value_from=kubernetes.client.V1EnvVarSource(
					field_ref=kubernetes.client.V1ObjectFieldSelector(field_path="metadata.namespace"))),
			kubernetes.client.V1EnvVar("HTTP_PORT", "4140"),
			kubernetes.client.V1EnvVar("HTTP_INGRESS_PORT", "4142"),
			kubernetes.client.V1EnvVar("GRPC_PORT", "5150"),
			kubernetes.client.V1EnvVar("ui_proxy", "$(NODE_NAME):$(HTTP_INGRESS_PORT)"),
			kubernetes.client.V1EnvVar("FLUENTD_HOST", "$(NODE_NAME)"),
			kubernetes.client.V1EnvVar("FLUENTD_PORT", "31111"),
		]

	def _get_job_env_vars(self, dobj):

		env_vars_list = self._get_default_env_vars(dobj)
		try:
			for conf_key, conf_value in dobj['serviceConfigs'].items():
				if isinstance(conf_value, dict):
					if 'value' in conf_value:
						env_vars_list.append(
								kubernetes.client.V1EnvVar(
										self._sanitize_env_var_name(conf_key),
										self._sanitize_env_var_value(conf_value['value']))
						)
				else:
					env_vars_list.append(kubernetes.client.V1EnvVar(
							self._sanitize_env_var_name(conf_key),
							self._sanitize_env_var_value(conf_value))
					)
		except Exception as ex:
			self._write_to_log(f"Error during generating Job configuration. {ex}", dobj['did'], CLog.ERROR)
			raise ex
		return env_vars_list

	def _construct_hook_job_body(self, dobj, image, job_name):
		container = kubernetes.client.V1Container(
				name=f"{dobj['serviceName']}-job",
				image=image,
				env=self._get_job_env_vars(dobj),
				# command=["/bin/bash", "-c",
				# 		 "echo $ORGMODEL_ELASTIC_URL; echo $ORGMODEL_CASSANDRA_CASAWS_KEYSPACE; echo 'Hello World 2'; echo 'Hello World 3'; sleep 3600;"]
		)
		pod_template = kubernetes.client.V1PodTemplateSpec(
				metadata=kubernetes.client.V1ObjectMeta(labels={"did": dobj['did']}),
				spec=kubernetes.client.V1PodSpec(containers=[container], restart_policy="Never")
		)
		spec = kubernetes.client.V1JobSpec(template=pod_template)
		metadata = kubernetes.client.V1ObjectMeta(name=job_name, labels={"did": dobj['did']})
		return kubernetes.client.V1Job(
				api_version="batch/v1",
				kind="Job",
				metadata=metadata,
				spec=spec
		)

	def create_job(self, dobj, image, job_name_suffix=""):
		job_name = f"{dobj['serviceName']}-{job_name_suffix}"
		# Construct K8S Job body
		job_body = self._construct_hook_job_body(dobj, image, job_name)
		# Delete any old jobs if exists
		self.delete_job(job_name)
		try:
			self._write_to_log(f"Creating new Job {job_name} for {image}", dobj['did'])
			self.batch_api.create_namespaced_job(KubeClient.NS, job_body)
			self._write_to_log(f"JOB {dobj['serviceName']} has been successfully created", dobj['did'], CLog.SUCCESS)
		except Exception as ex:
			self._write_to_log(f"Unable create new job for {job_name}, ex: {ex}", dobj['did'], CLog.ERROR)
			raise ex

		log_watch = watch.Watch()

		log_fetcher_thread = threading.Thread(target=self.watch_and_stream_logs, args=(log_watch, dobj['did'],))
		job_watcher = threading.Thread(target=self.watch_for_job_state, args=(dobj['did'], job_name, log_watch))

		try:
			log_fetcher_thread.start()
		except Exception as ex:
			self._write_to_log(f"Error during watching on Job: {job_name} pod logs , ex: {ex}", dobj['did'], CLog.ERROR)
			raise ex
		try:
			job_watcher.start()
		except Exception as ex:
			self._write_to_log(f"Error during watching on job {job_name}, ex: {ex}", dobj['did'], CLog.ERROR)
			raise ex

		log_fetcher_thread.join()
		job_watcher.join()

	def watch_and_stream_logs(self, watch, did):
		ls = f"did={did}"
		for e in watch.stream(self.core_api.list_pod_for_all_namespaces, label_selector=ls, watch=True):
			job_pod = e['object']
			if job_pod.status.phase == 'Running':
				res = self.core_api.read_namespaced_pod_log(
						job_pod.metadata.name,
						KubeClient.NS,
						_preload_content=False,
						follow=True)
				for chunk in res.stream(1024):
					if chunk:
						self._write_to_log("[HOOK-STDOUT] %s" % chunk.decode("utf-8"), did)
			if job_pod.status.phase == 'Succeeded':
				self._write_to_log("Job succeeded!", did)
				watch.stop()
			CLog.log(f"Event: {e['type']} | Phase: {job_pod.status.phase} ")

	def watch_for_job_state(self, did, job_name, log_watch):
		ls = f"did={did}"
		w = watch.Watch()
		for e in w.stream(self.batch_api.list_namespaced_job, KubeClient.NS, label_selector=ls, watch=True):
			job_status = e['object'].status
			CLog.log(job_status)

			if job_status.active == 1:
				self._write_to_log(f"Job {job_name} is still running. . . ", did)
			if job_status.succeeded == 1:
				self._write_to_log(f"Hook {job_name} succeeded", did, CLog.SUCCESS)
				self.set_hook_exec_result(BK8SHooks.HOOK_SUCCESS_RESULT)
				w.stop()
			if job_status.failed == 3:
				self._write_to_log(f"Hook {job_name} is failed! Gonna kill the Job. . .", did, CLog.ERROR)
				log_watch.stop()
				self.delete_job(job_name)
				self.set_hook_exec_result(BK8SHooks.HOOK_ERROR_RESULT)
				w.stop()


class KubeCTLClient(BaseDeploy):
    def __init__(self, did):
        super(KubeCTLClient, self).__init__(did)

    def get_deployment(self):
        cmd = self._construct_cmd("deployment")
        return self._exec_cmd(cmd)

    def get_services(self):
        cmd = self._construct_cmd("service")
        return self._exec_cmd(cmd)

    def get_jobs(self):
        cmd = self._construct_cmd("job")
        return self._exec_cmd(cmd)

    def get_secrets(self):
        cmd = self._construct_cmd("secret")
        return self._exec_cmd(cmd)

    def _construct_cmd(self, resource):
        return """
        export KUBECONFIG={chart_workspace}/config; 
        {kubectl} get {resource} --output=yaml -l "did={did}"
        """.format(
                chart_workspace=self.chart_workspace,
                kubectl=conf.KUBECTL,
                did=self.did,
                resource=resource
        )

    def _exec_cmd(self, cmd):
        cmd_exit_status, stdout, stderr = self.exec_shell_cmd(cmd, True)
        CLog.log(stdout)
        CLog.log(stderr, CLog.ERROR)
        if cmd_exit_status:
            msg = "Error during cmd execution %s" % cmd
            CLog.log(msg, CLog.ERROR)
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, msg)
        return stdout


class HelmClient(BaseDeploy):
    def __init__(self, did):
        super(HelmClient, self).__init__(did)
        self.set_kube_config()

    def set_kube_config(self):
        try:
            self._write_to_log("Dumping environment kubeconfig file")
            with open("%s/config" % self.chart_workspace, "wb") as f:
                f.write(base64.b64decode(self.dobj['env']['kubeconfig']))
            f.close()
        except Exception as ex:
            self.handel_error("Error during writing kubeconfig file")

    def get_helm_kubeconfig(self):
        return "export KUBECONFIG={chart_workspace}/config".format(chart_workspace=self.chart_workspace)

    def add_repo_and_update(self):
        add_repo_cmd = "{helm} repo add {repo_name} {repo_addr}".format(
                helm=self.helm,
                repo_name=self.dobj['chartRepo']['repoName'],
                repo_addr=self.dobj['chartRepo']['repoUrl']
        )
        self._exec_cmd(add_repo_cmd)
        update_cmd = "{helm} repo update".format(helm=self.helm)
        self._exec_cmd(update_cmd)

    def fetch_helm_package(self):
        cmd = "{helm} fetch {repo_name}/{chart_name} --version {version} --destination {destination} --untar".format(
                helm=self.helm,
                repo_name=self.dobj['chartRepo']['repoName'],
                chart_name=self.dobj['chartName'],
                version=self.dobj['version'],
                destination=self.chart_workspace
        )
        self._exec_cmd(cmd)

    def get_helm_init(self):
        return "{helm} init".format(helm=self.helm)

    def install_dry_run(self, configs=''):
        """
        execute helm install --dry-run --debug -n chart_name --set did=uid chart_name
        :return: stdout string
        """
        cmd = "{helm} install --set did={did} --set issuer={issuer} --set tsver={tsver} {configs} --dry-run --debug {chart}".format(
                helm=self.helm,
                chart=self.chart_dir,
                did=self.did,
                issuer=CG.ui.email,
                tsver=self._get_release_version(),
                configs=configs

        )
        return self._exec_cmd(cmd)

    def install(self, configs=''):
        """
        execute: helm install -n chart_name --set did=uid chart_name
        :return: None
        """
        cmd = "{helm} install -n {release_name} --set did={did} --set issuer={issuer} --set tsver={tsver} {configs} {chart}".format(
                helm=self.helm,
                release_name=self._get_release_name(),
                did=self.did,
                configs=configs,
                chart=self.chart_dir,
                issuer=self._get_issuer(),
                tsver=self._get_release_version()
        )
        self._exec_cmd(cmd)

    def upgrade(self, configs=''):
        """
        execute: helm upgrade --set did=uid release_name chart_path
        :return: None
        """
        cmd = "{helm} upgrade --set did={did} --set issuer={issuer} --set tsver={tsver} {configs} {release_name} {chart_path}".format(
                helm=self.helm,
                did=self.dobj['did'],
                issuer=self._get_issuer(),
                tsver=self._get_release_version(),
                configs=configs,
                release_name=self._get_release_name(),
                chart_path=self.chart_dir
        )

        self._exec_cmd(cmd)

    def delete(self):
        """
        execute: helm delete --purge chart_name
        Delete and purge Helm Chart release
        :return: None
        """
        release_name = self._get_release_name()
        cmd = "{helm} delete --purge {release_name}".format(helm=self.helm, release_name=release_name)
        self._exec_cmd(cmd)

    def list(self):
        """
        execute helm ls
        :return: list of releases
        """
        cmd = "{helm} list --all -q".format(helm=self.helm)
        releases = self._exec_cmd(cmd)
        return releases.split("\n")

    def package(self):
        """
        execute helm package chart_path-d dest_folder
        :return: None
        """
        cmd = "{helm} package {chart_path} -d {dest_folder}".format(
                helm=self.helm,
                chart_path=self.chart_dir,
                dest_folder=self.chart_workspace
        )
        self._exec_cmd(cmd)

    def _get_release_name(self):
        release_name = None
        dtype = detect_installation_type(self.did)
        if dtype == DeploymentType.SERVICE:
            release_name = self.dobj['deployServiceName']
        if dtype == DeploymentType.CHART:
            release_name = self.dobj['chartName']
        return release_name

    def _get_release_version(self):
        release_version = None
        dtype = detect_installation_type(self.did)
        if dtype == DeploymentType.SERVICE:
            release_version = self.dobj['dockerProperties']['dockerTag'].replace(".", "-")
        if dtype == DeploymentType.CHART:
            release_version = self.dobj['version'].replace(".", "-")
        return release_version

    def _get_issuer(self):
        return CG.ui.email[0:CG.ui.email.find("@")]

    def _patch_cmd(self, cmd):
        return "%s; %s; %s;" % (self.get_helm_kubeconfig(), self.get_helm_init(), cmd)

    def _exec_cmd(self, cmd):
        """
        Patch and executed Chart Helm shell cmd
        :param cmd: string helm chart cmd
        :return: stdout or raise exception
        """
        # Patch the cmd with the EXPORT KUBECONFIG prefix
        cmd = self._patch_cmd(cmd)
        # Execute shell cmd
        cmd_exit_status, stdout, stderr = self.exec_shell_cmd(cmd, True)
        self._write_to_log(stdout)
        if stderr:
            self._write_to_log(stderr, CLog.ERROR)

        if cmd_exit_status:
            self.handel_error("Error during cmd execution %s" % cmd)
        return stdout


class HelmRepoClient(object):
    def __init__(self, env_name, repo_name):
        CLog.log("Init HelmRepoClient with %s repo" % repo_name)
        self.repo_url = self._get_repo_url(env_name, repo_name)
        self._index = self._fetch_repo_index()

    def _get_repo_url(self, env_name, repo_name):
        env = DPEnv().get_env(env_name)
        for repo in env['helmRepo']:
            if repo['repoName'] == repo_name:
                return repo['repoUrl']
        raise CException(falcon.HTTP_404, "Helm repo %s name not found in env %s" % (repo_name, env_name))

    def _fetch_repo_index(self):
        try:
            r = requests.get(self.repo_url).text
            index = yaml.load(r)
            if 'entries' not in index:
                msg = "The Repo %s index entries are empty or doesn't exists" % self.repo_url
                CLog.log(msg, CLog.ERROR)
                raise Exception()
            return index
        except Exception as ex:
            CLog.log("Error during fetching/parsing repo %s index ex: %s" % (self.repo_url, ex), CLog.ERROR)
            CException(falcon.HTTP_400, CException.ERROR_STATUS,
                       "Error during fetching/parsing repo %s index" % self.repo_url)

    def list_chart_versions(self, chart_name):
        chart_versions = {'chart_name': chart_name, 'versions': []}
        if chart_name not in self._index['entries']:
            msg = "Chart %s not found in repo"
            CLog.log(msg, CLog.ERROR)
            raise CException(falcon.HTTP_404, CException.ERROR_STATUS, msg)

        for chart_version in self._index['entries'][chart_name]:
            chart_versions['versions'].append(chart_version['version'])
        return chart_versions

    def list_charts(self):
        repo_charts = []
        for chart_name, _ in self._index['entries'].items():
            repo_charts.append(chart_name)
        return repo_charts
