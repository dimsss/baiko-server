import kubernetes.client
from kubernetes import config
from baiko_server.config import conf
from pyper.logger import CLog
from pyper.exception import CException
from abc import ABC, abstractmethod

from baiko_server.common.api_clients import KubeClient, KubeCTLClient, BK8SHooks
from kubernetes.client.rest import ApiException
from baiko_server.common.data_provider import DPDObj


class L5DPSHook(object):
	def __init__(self, env):
		self.env = env
		self.kc = KubeClient(self.env)

	def run_hook(self, namespace="default"):
		if not conf.DELETE_L5D_PODS:
			return

		body = kubernetes.client.V1DeleteOptions()
		try:
			CLog.log("Trying to delete L5D pods")
			pods = self.kc.core_api.list_namespaced_pod(namespace, label_selector="app=l5d")
			for pod in pods.items:
				name = pod.metadata.name
				response = self.kc.core_api.delete_namespaced_pod(name, namespace, body)
				CLog.log("Pod %s deleted" % name)
		except ApiException as e:
			CLog.log(e, level=CLog.ERROR)


class HookBase(ABC):
	@abstractmethod
	def run_hook(self):
		pass

	@abstractmethod
	def should_run(self):
		pass

	def write_log(self, msg, did, level=CLog.INFO):
		CLog.log(msg, level, None, (DPDObj().append_to_log, did))


class DeploymentHookExecutor(object):
	HOOK_PRE_INSTALL = 'pre-install'
	HOOK_POST_INSTALL = 'post-install'
	HOOK_PRE_DELETE = 'pre-delete'
	HOOK_POST_DELETE = 'post-delete'

	def __init__(self, did):
		self.did = did
		self.dobj = DPDObj().get_dobj(did)

	def exec_pre_deployment_hook(self):
		if self._should_run(DeploymentHookExecutor.HOOK_PRE_INSTALL):
			# Execute the hook, and return True in case of hook success
			# or False in case of hook failure.
			self._write_log(f"************ Executing hook {DeploymentHookExecutor.HOOK_PRE_INSTALL} ************")
			hook_res = self._run_hook(DeploymentHookExecutor.HOOK_PRE_INSTALL)
			self._write_log(f"************ Finished hook {DeploymentHookExecutor.HOOK_PRE_INSTALL} ************")
			return hook_res
		else:
			# No need to run the hook, all good, return True
			return True

	def exec_post_deployment_hook(self):
		if self._should_run(DeploymentHookExecutor.HOOK_POST_INSTALL):
			# Execute the hook, and return True in case of hook success
			# or False in case of hook failure.
			self._write_log(f"Executing hook {DeploymentHookExecutor.HOOK_POST_INSTALL}", CLog.SUCCESS)
			hook_res = self._run_hook(DeploymentHookExecutor.HOOK_POST_INSTALL)
			self._write_log(f"Finished hook {DeploymentHookExecutor.HOOK_POST_INSTALL}", CLog.SUCCESS)
			return hook_res
		else:
			# No need to run the hook, all good, return True
			return True

	def _should_run(self, hook_name):
		# User may disable/enable deployment hooks execution globally
		# in configs, if DEPLOYMENT_HOOKS is False, skip all hooks
		if conf.DEPLOYMENT_HOOKS is False:
			CLog.log("Hooks disabled in configs, gonna skip ALL HOOKS execution! ")
			return False
		if hook_name in self.dobj['dockerProperties']:
			CLog.log(f"Gonna run {hook_name}")
			return True
		else:
			CLog.log(f"Gonna skip {hook_name}", CLog.WARNING)
			return False

	def _run_hook(self, hook_name):
		docker_image = self.dobj['dockerProperties'][hook_name]
		self._write_log(f"Hook Docker Image: {docker_image}", self.dobj['did'])
		baiko_k8s_hooks = BK8SHooks(self.dobj['env']['envName'])
		baiko_k8s_hooks.create_job(self.dobj, docker_image, hook_name)
		hook_res = baiko_k8s_hooks.get_hook_exec_result()
		if hook_res == BK8SHooks.HOOK_SUCCESS_RESULT:
			return True
		if hook_res == BK8SHooks.HOOK_ERROR_RESULT or hook_res == BK8SHooks.HOOK_UNKNOWN_RESULT:
			return False

	def _write_log(self, msg, level=CLog.INFO):
		CLog.log(msg, level, None, (DPDObj().append_to_log, self.did))
