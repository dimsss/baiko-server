import falcon
from pyper.exception import CException

from pyper.logger import CLog
from .utils import dobj_update_n_sync
import base64
import tarfile
from .base_deploy import BaseDeploy
from .api_clients import KubeClient, HelmClient, VaultClient
import yaml


class ChartDeployer(BaseDeploy):
	def __init__(self, did):
		super(ChartDeployer, self).__init__(did)
		self.helm_client = HelmClient(self.did)

	def check_if_helm_release_exists(self):
		if self.dobj['chartName'] in self.helm_client.list():
			return True
		return False

	def fetch_chart_deploy_configs(self):
		vault = VaultClient(self.dobj['env']['envName'])
		configs = vault.fetch_service_configs(self.dobj['chartName'])
		chart_config_str = ""
		if configs:
			for config_key, config_value in configs.items():
				chart_config_str += "--set {key}={value} ".format(key=config_key, value=config_value)
		return chart_config_str

	def deploy(self):
		self._write_to_log("************ Starting Helm Chart deployment ************", CLog.SUCCESS)
		self.helm_client.add_repo_and_update()
		self.helm_client.fetch_helm_package()
		chart_configs = self.fetch_chart_deploy_configs()
		helm_dry_run_output = self.helm_client.install_dry_run(chart_configs)
		dobj_update_n_sync(self.dobj, {'chart_dry_run': helm_dry_run_output.encode()})

		if self.check_if_helm_release_exists():
			self._write_to_log("*** Gonna upgrade existing Chart ***", CLog.SUCCESS)
			self.helm_client.upgrade(chart_configs)
		else:
			self._write_to_log("*** Gonna install new Chart ***", CLog.SUCCESS)
			self.helm_client.install(chart_configs)
		self._write_to_log("************ Successfully finished Helm Chart deployment ************", CLog.SUCCESS)

	def delete_chart(self):
		self.helm_client.delete()


class ServiceDeployer(BaseDeploy):
	def __init__(self, did):
		super(ServiceDeployer, self).__init__(did)
		self.helm_client = HelmClient(self.did)

	def dump_chart_from_pg(self):
		dobj_update_n_sync(self.did, {'steps': 'Downloading Helm Chart from MongoDB'})
		chart_content = self.dobj['chart']
		if not chart_content:
			raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "Helm chart is missing for DID: %" % self.did)
		chart_path = "%s/%s" % (self.chart_workspace, self.dobj['chartName'])
		self._write_to_log("Gonna download Helm Chart package and store here %s" % chart_path)
		with open(chart_path, "wb") as f:
			f.write(base64.b64decode(chart_content))
		f.close()
		self._write_to_log("Gonna extract the Helm Chart package archive")
		tar = tarfile.open(chart_path)
		tar.extractall(self.chart_workspace)
		tar.close()

	def delete_chart(self):
		self.helm_client.delete()

	def check_if_helm_release_exists(self):
		if self.dobj['deployServiceName'] in self.helm_client.list():
			return True
		return False

	def check_if_deployment_type_stateful_set(self):
		dep_obj = yaml.safe_load(self.dobj['depObj'])
		if dep_obj['service']['deployment']['type'] == 'StatefulSet':
			return True
		return False

	def deploy(self):
		self._write_to_log("************ Starting Helm Chart deployment ************", CLog.SUCCESS)
		self.dump_chart_from_pg()
		kc = KubeClient(self.dobj['env']['envName'])
		if kc.is_secret_exists(self.dobj.get('deployServiceName')):
			kc.delete_service_secrets(self.dobj.get('deployServiceName'))

		kc.generate_k8s_secret_as_env_vars(self.dobj)
		kc.generate_k8s_secret_as_file(self.dobj)
		self.helm_client.install_dry_run()
		# For now, HELM doesn't support StatefulSet upgrade, as a result, the whole release
		# should be deleted and installed from scratch
		# TODO: Add support for StatefulSet upgrade

		if self.check_if_deployment_type_stateful_set() and self.check_if_helm_release_exists():
			self._write_to_log("Detected StatefulSet release, gonna completely remove it and reinstall from scratch",
							   CLog.SUCCESS)
			self.delete_chart()

		if self.check_if_helm_release_exists():
			self._write_to_log("*** Gonna upgrade existing Chart ***", CLog.SUCCESS)
			self.helm_client.upgrade()
		else:
			self._write_to_log("*** Gonna install new Chart ***", CLog.SUCCESS)
			self.helm_client.install()
		self._write_to_log("************ Successfully finished Helm Chart deployment ************", CLog.SUCCESS)
