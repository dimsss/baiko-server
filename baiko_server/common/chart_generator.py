import os
import shutil

import yaml
from baiko_server.config import conf
from baiko_server.common.utils import calculate_grpc_gw_ports
from pyper.logger import CLog
from .api_clients import ArtifactoryClient, SSHClient, HelmClient, VaultClient
import requests
from pyper.exception import CException
import falcon
from .utils import dobj_update_n_sync
from .data_provider import DPDObj
import base64
from .base_deploy import BaseDeploy


class ChartGenerator(BaseDeploy):
    def __init__(self, did):
        super(ChartGenerator, self).__init__(did)
        self._write_to_log("Gonna generate chart...")
        self._fetch_docker_manifest()
        self.dep_obj = self._load_dep_file_into_obj()
        self.chart_tmpl_dir = "%s/../res/chart_tmpl" % os.path.dirname(os.path.abspath(__file__))
        self.helm_client = HelmClient(did)

    def _fetch_docker_tag(self):
        # If docker tag is not latest, return tag as is
        if self.dobj['dockerTag'] != "latest":
            return
        # If docker is latest, fetch all tags, sort the tags and update dobj to the heights one
        artifactory_url = "%s/artifactory/api" % self.dobj['env']['artifactoryBaseUrl']
        ac = ArtifactoryClient(artifactory_url, self.dobj['dockerRegistry'], self.dobj['serviceName'])
        res = ac.get_image_tags()
        if 'tags' in res and len(res['tags']):
            sorted_tags = sorted(res['tags'])
            self.dobj = dobj_update_n_sync(self.dobj, {'steps': 'Fetch docker tag', 'dockerTag': sorted_tags[len(sorted_tags) - 1]})
        else:
            self._write_to_log("No tags has been found for %s" % self.dobj['serviceName'], CLog.ERROR)
            dobj_update_n_sync(self.dobj, {'status': 'error', 'errorMessage': 'Tag not found'})
            raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "Tag not found, did: " % self.did)

    def _fetch_docker_manifest(self):
        artifactory_url = "%s/artifactory/api" % self.dobj['env']['artifactoryBaseUrl']
        ac = ArtifactoryClient(artifactory_url, self.dobj['dockerRegistry'], self.dobj['serviceName'])
        self._write_to_log("Gonna fetch properties JSON file for %s:%s" % (self.dobj['serviceName'], self.dobj['dockerTag']))
        image_props = ac.get_docker_manifest(self.dobj['dockerRegistry'], self.dobj['serviceName'], self.dobj['dockerTag'])
        if image_props:
            dobj_update_n_sync(self.dobj, {'dockerProperties': image_props})
        else:
            self._write_to_log("Failed to fetch Docker property.json file from artifactory")
            dobj_update_n_sync(self.dobj, {'status': 'error', 'errorMessage': "Failed to fetch Docker property.json file from artifactory"})
            raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "Failed to fetch Docker property.json file from artifactory")

    def _load_dep_file_into_obj(self):
        self._write_to_log("Trying to load dep file from")
        try:
            # Download the dep.file from artifactory
            depfile, dep_obj = self.get_dep_file()
            self.dobj = dobj_update_n_sync(self.dobj, {'steps': 'Fetched dependency file', 'depObj': depfile})
            dep_obj['service']['name'] = self.dobj['deployServiceName']
            return dep_obj
        except Exception as ex:
            dobj_update_n_sync(self.dobj, {'status': 'error', 'errorMessage': 'Error while loading/parsing from YAML to obj'})
            self._write_to_log("Error while loading/parsing from YAML to obj", CLog.ERROR)
            raise ex

    def _copy_chart_tmpl(self):
        dobj_update_n_sync(self.dobj, {'steps': 'Copy chart template'})
        # Make sure always create a new fresh directory of chart, even if it's already exists
        if os.path.exists(self.chart_dir):
            self._write_to_log("Chart directory already exists %s, gonna remove it and create a new one" % self.chart_dir)
            shutil.rmtree(self.chart_dir)
        self._write_to_log("Copy Chart template from %s " % self.chart_tmpl_dir)
        shutil.copytree(self.chart_tmpl_dir, self.chart_dir)

    def _parse_chart_data_file(self, file_name):
        try:
            with open("%s/%s" % (self.chart_dir, file_name)) as f:
                file_context = f.read()
            f.close()
            return yaml.load(file_context)
        except Exception as ex:
            dobj_update_n_sync(self.dobj, {'status': 'error', 'errorMessage': "Unable to parse %s/%s" % (self.chart_dir, file_name)})
            self._write_to_log("Unable to parse %s/%s" % (self.chart_dir, file_name), CLog.ERROR)
            raise ex

    def _dump_chart_data(self, data, file_name):
        try:
            file_path = "%s/%s" % (self.chart_dir, file_name)
            self._write_to_log("Gonna dump %s to %s" % (file_name, file_path))
            with open(file_path, "w") as f:
                yaml.dump(data, f, default_flow_style=False)
            f.close()
        except Exception as ex:
            dobj_update_n_sync(self.dobj, {'status': 'error', 'errorMessage': "Unable to write into %s/%s" % (self.chart_dir, file_name)})
            self._write_to_log("Unable to write into %s/%s" % (self.chart_dir, file_name))
            raise ex

    def _gen_chart_file(self, file_name="Chart.yaml"):
        dobj_update_n_sync(self.dobj, {'steps': "Generating Chart.yaml file"})
        chart = self._parse_chart_data_file(file_name)
        chart['description'] = "Auto generated Helm Chart by Baiko tool"
        chart['name'] = self.dobj['deployServiceName']
        chart['version'] = self.dobj['dockerProperties']['dockerTag']
        self._dump_chart_data(chart, file_name)

    def _gen_values_file(self, file_name="values.yaml"):
        dobj_update_n_sync(self.dobj, {"steps": "Generating Values.yaml file"})
        try:
            values = self._parse_chart_data_file(file_name)
            values['service'] = self.dep_obj['service']
            values['application_name'] = self.dobj['deployServiceName']
            values['k8s_name'] = self.dobj['env']['envName']
            # Image section
            values['image']['repository'] = "%s/%s" % (self.dobj['dockerRegistry'], self.dobj['serviceName'])
            values['image']['tag'] = self.dobj['dockerProperties']['dockerTag']
            # Port section
            values['resources'] = self.dep_obj['resources']
            if 'ops' in self.dep_obj:
                # OPS section
                values['ops'] = self.dep_obj['ops']
            # GRPC Gateway
            self._calculate_grpc_gw_ports(values)
            values['pod']['annotations'] = self._get_k8s_annotations(self.dobj['serviceName'], self.dobj['serviceRole'], values['k8s_name'])

            self._dump_chart_data(values, file_name)
        except Exception as ex:
            dobj_update_n_sync(self.dobj, {'status': 'error', 'errorMessage': 'Failed to generate Chart Values.yaml file'})
            self._write_to_log("Failed to generate Chart Values.yaml file", CLog.ERROR)
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Failed to generate Chart Values.yaml file")

    def _get_k8s_annotations(self, service, role, env):
        vc = VaultClient(env)
        res = vc.fetch_service_annotations(service, role)
        return res

    def _calculate_grpc_gw_ports(self, values):
        if 'grpcGateway' in values['resources']:
            for port in values['resources']['ports']:
                if port['name'] == values['resources']['grpcGateway']['GW_INC_HTTP']:
                    values['resources']['grpcGateway']['GW_INC_HTTP'] = port['port']
                elif port['name'] == values['resources']['grpcGateway']['GW_INC_HTTP_OPS']:
                    values['resources']['grpcGateway']['GW_INC_HTTP_OPS'] = port['port']
                elif port['name'] == values['resources']['grpcGateway']['GW_OUT_GRPC']:
                    values['resources']['grpcGateway']['GW_OUT_GRPC'] = port['port']
                elif port['name'] == values['resources']['grpcGateway']['GW_OUT_GRPC_OPS']:
                    values['resources']['grpcGateway']['GW_OUT_GRPC_OPS'] = port['port']
        else:
            values['resources']['grpcGateway'] = {'GW_INC_HTTP': 8080, 'GW_INC_HTTP_OPS': 8091, 'GW_OUT_GRPC': 5050, 'GW_OUT_GRPC_OPS': 5060}

    def _validate_chart(self):
        dobj_update_n_sync(self.dobj, {"steps": "Validating Chart"})
        helm_dry_run_output = self.helm_client.install_dry_run()
        dobj_update_n_sync(self.dobj, {'chart_dry_run': helm_dry_run_output})
        self._write_to_log("\n %s" % helm_dry_run_output)
        self._write_to_log("============ Chart dry run output end ============ ")

    def _pack_chart(self):
        dobj_update_n_sync(self.dobj, {"steps": "Pack Chart"})
        self.helm_client.package()

    def _push_chart_to_repo(self):
        dobj_update_n_sync(self.dobj, {"steps": "Push Chart"})
        ssh = SSHClient(conf.HELM_REPO_HOST)
        package_name = "%s-%s.tgz" % (self.dep_obj['service']['name'], self.dobj['dockerTag'])
        self._write_to_log("Gonna upload package %s to helm repo" % package_name)
        with open("%s/%s" % (self.chart_workspace, package_name), 'rb') as f:
            content = f.read()
        f.close()
        tmp_path_package = "/tmp/%s" % package_name
        repo_path_package = "/usr/share/nginx/html/tcharts/" + package_name
        self._write_to_log("Gonna copy created package to tmp folder")
        ssh.put_remote_file(tmp_path_package, content)
        self._write_to_log("Moving package to repo directory")
        ssh.exec_cmd_as_root("mv %s %s" % (tmp_path_package, repo_path_package))
        self._write_to_log("Regenerate repository index.yaml file")
        ssh.exec_cmd_as_root("helm repo index --url http://hert.ops.traiana.int /usr/share/nginx/html/tcharts")
        ssh.ssh_client.close()

    def get_dep_file(self):
        if 'depFiles' not in self.dobj['dockerProperties']:
            self.handel_error("Artifactory manifest.json doesn't have depFiles key")

        for dep in self.dobj['dockerProperties']['depFiles']:
            download_url = "{base_url}/{registry}/{service}/{tag}/{dep}".format(
                base_url="%s/artifactory" % self.dobj['env']['artifactoryBaseUrl'],
                registry=self.dobj['dockerRegistry'].split(".")[0],
                service=self.dobj['serviceName'],
                tag=self.dobj['dockerTag'],
                dep=dep)
            depfile = self.download_depfile(download_url)
            try:
                dep_obj = yaml.load(depfile)
            except Exception as ex:
                raise CException(falcon.HTTP_400, CException.ERROR_STATUS,
                                  "Failed to parse dep file %s %s" % (download_url, ex))
            # Check if profiles are match
            if dep_obj['service']['profile'] == self.dobj['env']['envProfile']:
                # Role is set for service, find proper dep file
                if self.dobj['serviceRole']:
                    if 'role' in dep_obj['service'] and dep_obj['service']['role']:
                        if self.dobj['serviceRole'] == dep_obj['service']['role']:
                            return depfile, dep_obj
                # Role is not set, once the profile is match return the dep file
                else:
                    return depfile, dep_obj
        # Is depfile and dep object wasn't found raise exception
        self.handel_error("Can't match profile and/or role between selected env %s and available dep files" % self.dobj['env']['envProfile'])

    def download_depfile(self, url):
        depfile = ''
        try:
            self._write_to_log("Gonna download dep file from: %s" % url)
            r = requests.get(url, stream=True)
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:
                    depfile += str(chunk, 'utf-8')
        except Exception as ex:
            self.handel_error("Unable to download dep file from %s" % url)
        return depfile

    def _push_chart_to_db(self):
        try:
            # Chart page name
            package_name = "%s-%s.tgz" % (self.dep_obj['service']['name'], self.dobj['dockerProperties']['dockerTag'])
            # Chart full path
            package_fullname = "%s/%s" % (self.chart_workspace, package_name)
            self._write_to_log("Gonna push package %s to DB" % package_fullname)
            # Read from archive
            with open(package_fullname, 'rb') as f:
                content = f.read()
            f.close()
            # Encode binary archive to base64 and save into mongoDB
            chart_encoded = base64.b64encode(content)
            dobj_update_n_sync(self.dobj, {'chart': str(chart_encoded, 'utf-8'), 'chartName': package_name})
        except Exception as ex:
            self.handel_error("Unable to package Helm Chart %s " % ex)

    def gen_chart(self):
        self._write_to_log("########### Starting chart construction ###########", CLog.SUCCESS)
        self._write_to_log("Copy chart from template into workspace")
        self._copy_chart_tmpl()
        self._gen_chart_file()
        self._gen_values_file()
        self._validate_chart()
        self._pack_chart()
        self._push_chart_to_db()
        dobj_update_n_sync(self.dobj, {"steps": "Finish chart construction"})
        self._write_to_log("########### Finish chart construction ###########", CLog.SUCCESS)

    def _write_to_log(self, msg, level=None):
        CLog.log(msg, level, None, (DPDObj().append_to_log, self.did))
        # CLog.log(msg, level, None, self.db, 'did', self.did, 'dobj')
