from baiko_server.config import conf
from saml2 import (BINDING_HTTP_POST, BINDING_HTTP_REDIRECT, entity)
from saml2.client import Saml2Client
from saml2.config import Config as Saml2Config
from pyper.exception import CLog, CException
import falcon
from pyper.auth import JWTGenerator
import jwt
from jwt import DecodeError, ExpiredSignature
from .data_provider import DPUser

import time


class OktaSamlAuthenticator(object):
    DEFAULT_ACCESS_GROUP = 'admin'

    def __init__(self, saml_response, mode, identity=None):
        self.saml_response = saml_response
        self.identity = identity
        self.authn_response = None
        self.mode = mode
        self.metadata = self.get_metadata()
        self.acs_url = "{url}/{mode}".format(url=conf.ACS_URL, mode=self.mode)
        self.secure_acs_url = "{url}/{mode}".format(url=conf.ACS_SECURE_URL, mode=self.mode)

    def get_metadata(self):
        if self.mode == 'api':
            return conf.SAML_METADATA_API
        if self.mode == 'ui':
            return conf.SAML_METADATA_UI

    def auth(self):
        """
        Execute SAML verification, if success, user authenticated 
        :return: True if user authenticated, False if not
        """
        try:
            spConfig = Saml2Config()
            spConfig.load(self.get_settings())
            spConfig.allow_unknown_attributes = True
            saml_client = Saml2Client(config=spConfig)
            self.authn_response = saml_client.parse_authn_request_response(self.saml_response, entity.BINDING_HTTP_POST)
            self.identity = self.authn_response.get_identity()
            return True
        except Exception as ex:
            CLog.log("Error while authenticating against SAML, ex: %s" % ex, CLog.ERROR)
            return False

    def gen_auth_token(self):
        """
        Create JWT token 
        :return: JWT token
        """
        identity_data = self.get_identity_data()
        # Set token expiration to 12h
        exp = time.time() + 43200
        try:
            user = self.check_if_user_exists(identity_data['email'])
            token = JWTGenerator(conf.JWT_SECRET, user['email'], conf.APP_NAME, exp, user).generate_token()
            return token
        except Exception as ex:
            CLog.log("Unable create JWT token, ex: %s" % ex)
            raise ex

    def gen_offline_auth_token(self):
        """
        create JWT offline token
        :return:
        """
        identity_data = self.get_identity_data()

        exp = 3155760000  # Wednesday, January 1, 2070 12:00:00 AM
        try:
            dpu = DPUser()
            user = dpu.get_user(identity_data.get('email'))
            if user.get('offline_token'):
                offline_token = user.get('offline_token')
            else:
                data = {'type': 'offline_token'}
                offline_token = JWTGenerator(jwt_secret=conf.JWT_SECRET, exp=exp, username=user.get('email'), app_name=conf.APP_NAME, data=data).generate_token()
                dpu.update_offline_token(user, offline_token)
            return offline_token
        except Exception as e:
            CLog.log('Unable create offline token: {}'.format(e))

    def get_offline_token(self):
        identity_data = self.get_identity_data()
        user = self.check_if_user_exists(identity_data.get('email'))
        offline_token = user.get('offline_token')
        if offline_token:
            return offline_token
        return None

    def check_if_user_exists(self, email):
        dpu = DPUser()
        user = dpu.get_user(email)
        identity_data = self.get_identity_data()
        if not user:
            dpu.create_user({
                'email': email,
                'accessGroup': OktaSamlAuthenticator.DEFAULT_ACCESS_GROUP,
                'preferences':
                    {
                        'firstName': identity_data['first_name'],
                        'lastName': identity_data['last_name'],
                    }
            })
            user = dpu.get_user(email)
            if not user:
                raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Unable create user")
        return user

    def get_identity_data(self):
        """
        Getting identity data as dict of first_name, last_name and email
        :return: dict of identity data 
        """
        try:
            return {
                'first_name': self.identity['FirstName'][0],
                'last_name': self.identity['LastName'][0],
                'email': self.identity['Email'][0]
            }
        except Exception as ex:
            CLog.log("Unable construct identity data, ex: %s" % ex)
            raise ex

    def get_settings(self):
        """
        Prepare SAML settings 
        :return: dict of SAML settings
        """
        return {
            'metadata': {
                'inline': [self.metadata],
            },
            'service': {
                'sp': {
                    'endpoints': {
                        'assertion_consumer_service': [
                            (self.acs_url, BINDING_HTTP_REDIRECT),
                            (self.acs_url, BINDING_HTTP_POST),
                            (self.secure_acs_url, BINDING_HTTP_REDIRECT),
                            (self.secure_acs_url, BINDING_HTTP_POST)
                        ],
                    },
                    'allow_unsolicited': True,
                    'authn_requests_signed': False,
                    'logout_requests_signed': True,
                    'want_assertions_signed': True,
                    'want_response_signed': False,
                },
            },
        }


class TokenVerifier(object):
    def __init__(self, token):
        self.token = token

    def get_token_details(self):
        try:
            # Try decode token
            token_data = jwt.decode(self.token, conf.JWT_SECRET, algorithms=JWTGenerator.ALGO)
            # Get token TTL in munutes
            token_ttl = int((token_data['exp'] - time.time()) / 60)
            # Get user email
            sub = token_data['sub']
            return {
                'token_ttl': token_ttl,
                'sub': sub,
                'accessGroup': token_data['accessGroup'],
                'preferences': token_data['preferences'],
                'token': self.token
            }
        except DecodeError as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, str(ex))
        except ExpiredSignature as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, str(ex))
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Token not valid")
