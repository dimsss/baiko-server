from baiko_server.config import conf
import requests
from pyper.logger import CLog
from pyper.exception import CException
from pyper.globals import IG
import falcon


class GitReposScanner(object):
    def __init__(self):
        self.projects = conf.GIT_APP_PROJECTS
        self.api_url = conf.GIT_API_URL
        CLog.log("GitRepoScanner API URL: %s, available projects: %s " % (self.api_url, self.projects))

    def fetch_repos_slugs(self, should_save=False):
        CLog.log("Fetching repos slugs from git")
        apps = []
        for project in self.projects:
            apps += self._fetch_slugs_for_project(project, 0, [])
        if should_save:
            self._save_apps(apps)
        return apps

    def _fetch_slugs_for_project(self, project_name, page_index=0, apps=[]):
        url = self.api_url % (project_name, page_index)
        # Exec API call to GIT and fetch all available repos under project
        # TODO: remove GTI user/pass, use instead read only system user!!!
        resp = requests.get(url, auth=('dmitrykar', 'Dima2014!!')).json()
        for repo in resp['values']:
            apps.append({'slug': repo['slug']})
        if 'nextPageStart' in resp:
            return self._fetch_slugs_for_project(project_name, resp['nextPageStart'], apps)
        return apps

    def fetch_intersec_slugs_apps(self):
        slugs_lst = []
        slugs = self.fetch_repos_slugs()
        for slug in slugs:
            slugs_lst.append(slug['slug'])
        apps_lst = []
        for app in IG.db_client[conf.DB_NAME].apps.find({}, {'appName': True, '_id': False}):
            apps_lst.append(app['appName'])
        return list(set(slugs_lst) - set(apps_lst))

    def _save_apps(self, apps: list):
        CLog.log("Trying to save application list to MongoDB")
        db = IG.db_client[conf.DB_NAME]
        # If apps collection already exists, make sure it's empty
        if 'apps' in db.collection_names():
            coll_stats = db.command('collStats', 'apps')
            if coll_stats['count'] > 0:
                raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Can't dump applications to DB since the apps collection is not empty")

        for app in apps:
            app_obj = {'appName': app['slug'], 'appDesc': '', 'roles': []}
            db.apps.insert_one(app_obj)

    def is_slug_exists(self, app_name):
        slugs = self.fetch_repos_slugs()
        slug_found = False
        for slug_obj in slugs:
            if slug_obj['slug'] == app_name:
                slug_found = True
        return slug_found
