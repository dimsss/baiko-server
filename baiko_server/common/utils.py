import subprocess
from baiko_server.config import conf
from pyper.logger import CLog
from pyper.globals import IG
from pyper.exception import CException
import falcon
from uuid import uuid4
import yaml, base64

HIDDEN_SECRET_VALUE = 5 * '*'


class AccessGroups(object):
	READ_LIMITED = "readLimited"
	READ = "read"
	WRITE = "write"
	ADMIN = "admin"
	SYSTEM = "system"


class DeploymentType(object):
	SERVICE = 'service'
	CHART = 'chart'


def exec_shell_cmd(cmd, return_stdout=False):
	if return_stdout:
		p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
	else:
		p = subprocess.Popen(cmd, shell=True)
	stdout, stderr = p.communicate()
	if stdout:
		stdout = str(stdout, 'utf-8')
	return p.returncode, stdout


def update_local_helm():
	cmd = "{helm} repo update".format(helm=conf.HELM_BIN)
	cmd_exit_code, stdout = exec_shell_cmd(cmd)
	if cmd_exit_code:
		CLog.log("Unable update local helm repo", CLog.ERROR)
		exit(1)


def generate_did(dtype):
	return "%s-%s" % (dtype, uuid4().hex)


def detect_installation_type(did):
	if did.find('service-') == 0:
		return 'service'
	if did.find('chart-') == 0:
		return 'chart'
	return 'service'


def fetch_dobj_by_did(did):
	from .data_provider import DPDObj, DPChartDObj
	dtype = detect_installation_type(did)
	if dtype == 'service':
		return DPDObj()
	if dtype == 'chart':
		return DPChartDObj()
	return DPDObj()


def dobj_update_n_sync(dobj, kv):
	"""
	Helper function to update and sync Installation object.
	The function updates document in MongoDB as well as update the current
	Installation object at self.
	:param dobj: Installation object at self, in case where the dobj is a string,
	e.g installation id, that function we'll try to fetch the installation object form DB
	 and  update it, and return entire updated installation object as a dict
	:param kv: The key value dict of what to update/insert into Installation object
	:return: dict, Installation object
	"""

	if not isinstance(dobj, dict):
		dpd_obj = fetch_dobj_by_did(dobj)
		dobj = dpd_obj.get_dobj(dobj)
		if not dobj:
			raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "DID not found")
	else:
		dpd_obj = fetch_dobj_by_did(dobj['did'])

	for item_key, item_value in kv.items():
		if item_key == 'steps':
			dpd_obj.append_to_step(dobj['did'], item_value)
		else:
			dpd_obj.update_by_key(dobj['did'], item_key, item_value)
		dobj[item_key] = item_value

	return dobj


def _add_env_suffix(payload):
	env_context = payload.copy()
	k8s_config_obj = yaml.load(base64.b64decode(env_context['kubeconfig']).decode('utf-8'))
	env_suffix = env_context['envName']

	k8s_config_obj['clusters'][0]['name'] += '-' + env_suffix
	k8s_config_obj['users'][0]['name'] += '-' + env_suffix

	k8s_config_obj['contexts'][0]['name'] = k8s_config_obj['users'][0]['name'] + '@' + k8s_config_obj['clusters'][0][
		'name']
	k8s_config_obj['contexts'][0]['context']['cluster'] = k8s_config_obj['clusters'][0]['name']
	k8s_config_obj['contexts'][0]['context']['user'] = k8s_config_obj['users'][0]['name']
	k8s_config_obj['current-context'] = k8s_config_obj['contexts'][0]['name']

	env_context['kubeconfig'] = base64.b64encode(
			bytes(yaml.dump(k8s_config_obj, default_flow_style=False), 'utf-8')).decode('utf-8')
	return env_context


def hide_secret_configs(configs_data: dict):
	if isinstance(configs_data, dict):
		configs = configs_data.copy()
		for key, val in configs.items():
			if isinstance(val, dict) and val.get('isHidden') is True:
				secret_key = configs.get(key)
				secret_key.update({'value': HIDDEN_SECRET_VALUE})
		return configs


def patch_hidden_values_in_configs(configs: dict, cleartext_configs: dict):
	if isinstance(configs, dict) and isinstance(cleartext_configs, dict):
		for key, val in configs.items():
			if isinstance(val, dict) and val.get('isHidden') is True and val.get('value') == HIDDEN_SECRET_VALUE:
				CLog.log("Found hidden value with starts ('*****'), gonna patch it")
				configs[key] = cleartext_configs.get(key)


def calculate_grpc_gw_ports(values: dict):
	"""
	Calculate GRPC Gateway ports
	:param values: depFile as dict
	:return: Mutate depFile obj (values) by setting GRPC Gateway ports
	"""
	if 'grpcGateway' in values['resources']:
		for port in values['resources']['ports']:
			if port['name'] == values['resources']['grpcGateway']['GW_INC_HTTP']:
				values['resources']['grpcGateway']['GW_INC_HTTP'] = port['port']
			elif port['name'] == values['resources']['grpcGateway']['GW_INC_HTTP_OPS']:
				values['resources']['grpcGateway']['GW_INC_HTTP_OPS'] = port['port']
			elif port['name'] == values['resources']['grpcGateway']['GW_OUT_GRPC']:
				values['resources']['grpcGateway']['GW_OUT_GRPC'] = port['port']
			elif port['name'] == values['resources']['grpcGateway']['GW_OUT_GRPC_OPS']:
				values['resources']['grpcGateway']['GW_OUT_GRPC_OPS'] = port['port']
	else:
		values['resources']['grpcGateway'] = {
			'GW_INC_HTTP':     8080,
			'GW_INC_HTTP_OPS': 8091,
			'GW_OUT_GRPC':     5050,
			'GW_OUT_GRPC_OPS': 5060
		}
