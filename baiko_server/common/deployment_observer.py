from .chart_generator import ChartGenerator
from .deployer import ServiceDeployer, ChartDeployer
from .hooks import DeploymentHookExecutor
from pyper.logger import CLog
from baiko_server.common.data_provider import DPDObj
from pyper.exception import CException
from .utils import fetch_dobj_by_did, detect_installation_type, DeploymentType
from .api_clients import VaultClient
from .data_provider import DPEnv
from .hooks import L5DPSHook
import datetime
import falcon


class DeploymentObserver(object):
	def __init__(self, did, irecord=None):
		"""
		Deployment observer class responsible for managing service Deployment flow
		:param did: Deployment ID
		:param irecord: Deployment record. To generate Deployment record use helper function from utils module get_drecord_template.
		"""
		self.did = did
		self.irecord = irecord
		self.dtype = detect_installation_type(self.did)
		self.dp = fetch_dobj_by_did(self.did)
		self._init_dobj()

	def _init_dobj(self):
		"""
		Create Deployment object if it's not exists
		:return: None
		"""
		CLog.log("Initiating Deployment Object")
		if not self.dp.is_dobj_exists(self.did):
			CLog.log("Deployment object has been not found, gonna create one")
			if self.dtype == DeploymentType.SERVICE:
				self._create_service_installation_record(**self.irecord)
			if self.dtype == DeploymentType.CHART:
				self._create_chart_installation_record(**self.irecord)

	def trigger_installation(self):
		if self.dtype == DeploymentType.SERVICE:
			self._trigger_service_installation()
		if self.dtype == DeploymentType.CHART:
			self._trigger_chart_installation()

	def _trigger_chart_installation(self):
		# SA chart deployment
		chart_deployer = ChartDeployer(self.did)
		chart_deployer.deploy()

	def _trigger_service_installation(self):
		"""
		Trigger service Deployment flow
		:return: None
		"""
		# Chart generator
		ch = ChartGenerator(self.did)
		ch.gen_chart()
		# Pre install Hook executor
		hook_executor = DeploymentHookExecutor(self.did)
		# If pre deployment hook successfully executed, proceed to deployment
		if hook_executor.exec_pre_deployment_hook() is True:
			# Generic chart deployer - as a part of service installation
			deployer = ServiceDeployer(self.did)
			deployer.deploy()
			hook_executor.exec_post_deployment_hook()
		else:
			CLog.log("Hook execution failed", CLog.ERROR, None, (DPDObj().append_to_log, self.did))
			raise Exception("Hook execution failed")

		# Delete L5D pods
		l5d_delete = L5DPSHook(self.irecord['env_name'])
		l5d_delete.run_hook()

	def _create_chart_installation_record(self, env_name, chart_repo, chart_name, chart_version):
		dp_env = DPEnv()
		env = dp_env.get_env(env_name)
		dobj = {
			'did':          self.did,
			'env':          env,
			'chartRepo':    self._get_chart_repo(chart_repo, env),
			'chartName':    chart_name,
			'version':      chart_version,
			'status':       'initiated',
			'errorMessage': None,
			'steps':        ['Initiating'],
			'log':          [],
			'date':         datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
		}
		self.dp.create_dobj(dobj)
		msg = """
        ******************** Chart Deployment ********************
                 {deployment_id:<30}{did}
                 {environment:<30}{env_name}
                 {repo:<30}{repo_name}
                 {chart:<30}{chart_name}
                 {version:<30}{chart_version}
                 ****************************************************""".format(
				deployment_id="Deployment ID: ",
				environment="Environment: ",
				repo="Repo: ",
				chart="Chart: ",
				version="Version: ",
				env_name=env_name,
				repo_name=chart_repo,
				chart_name=chart_name,
				chart_version=chart_version,
				did=self.did,
		)

		CLog.log(msg, CLog.SUCCESS, None, (self.dp.append_to_log, self.did))

	def _create_service_installation_record(self, env_name, service_name, docker_registry, docker_tag,
											service_role=None):
		"""
		Creating actual Deployment record in MongoDB
		:param env_name:
		:param service_name:
		:param docker_registry:
		:param docker_tag:
		:return: None
		"""
		vc = VaultClient(env_name)
		service_configs = vc.fetch_service_configs(service_name, service_role)
		dp_env = DPEnv()
		env = dp_env.get_env(env_name)

		dobj = {
			'did':               self.did,
			'env':               env,
			'deployServiceName': self.get_deploy_service_name(service_name, service_role),
			'serviceName':       service_name,
			'dockerRegistry':    docker_registry,
			'dockerTag':         docker_tag,
			'serviceRole':       service_role,
			'status':            'initiated',
			'errorMessage':      None,
			'steps':             ['Initiating'],
			'log':               [],
			'serviceConfigs':    service_configs,
			'date':              datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
		}
		self.dp.create_dobj(dobj)

		msg = """
        ******************** Deployment ********************
                 {deployment_id:<30}{did}
                 {environment:<30}{env_name}
                 {service:<30}{service_name}
                 {role:<30}{service_role}
                 {docker_reg:<30}{docker_registry}
                 {dt:<30}{docker_tag}
                 ****************************************************""".format(
				deployment_id="Deployment ID: ",
				environment="Environment: ",
				service="Service: ",
				docker_reg="Docker Registry: ",
				dt="Docker Tag: ",
				role="Service Role:",
				env_name=env_name,
				service_name=service_name,
				docker_registry=docker_registry,
				docker_tag=docker_tag,
				did=self.did,
				service_role=service_role)

		CLog.log(msg, CLog.SUCCESS, None, (self.dp.append_to_log, self.did))

	def get_deploy_service_name(self, service_name, service_role):
		if service_role:
			return "%s-%s" % (service_name, service_role)
		else:
			return service_name

	def _get_chart_repo(self, chart_repo, env):
		for repo in env['helmRepo']:
			if repo['repoName'] == chart_repo:
				return repo
		raise CException(falcon.HTTP_400, "Unable to find helm repo", CException.ERROR_STATUS)

	# for repo_name, repo_addr in env['helmRepo'].items():
	#     if repo_addr == chart_repo:
	#         return {'repo_name': repo_name, 'repo_addr': repo_addr}
	# raise CException(falcon.HTTP_400, "Unable to find helm repo", CException.ERROR_STATUS)

	def delete_deployment(self):
		if self.dtype == DeploymentType.SERVICE:
			self._delete_service_deployment()
		if self.dtype == DeploymentType.CHART:
			self._delete_chart_deployment()

	def _delete_service_deployment(self):
		deployer = ServiceDeployer(self.did)
		deployer.delete_chart()

	def _delete_chart_deployment(self):
		chart_deployer = ChartDeployer(self.did)
		chart_deployer.delete_chart()

	def rollback_service(self):
		"""
		Rollback to already existing service in deployment objects
		:return: None
		"""

		deployer = ServiceDeployer(self.did)
		deployer.deploy()
