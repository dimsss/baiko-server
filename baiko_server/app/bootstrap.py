from baiko_server.common.data_provider import DPEnv, DPBaikoConfigs
from pyper.globals import IG
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy import exc, event, select, create_engine
from pyper.logger import CLog
import os
import yaml
import base64
from .resources import *

engine = create_engine(
    'postgresql://%s:%s@%s:%s/%s' % (conf.DB_USER, conf.DB_PASS, conf.DB_HOST, conf.DB_PORT, conf.DB_NAME))
session_factory = sessionmaker(bind=engine)
IG.DBSession = scoped_session(session_factory)


@event.listens_for(engine, "engine_connect")
def ping_connection(connection, branch):
	if branch:
		return
	save_should_close_with_result = connection.should_close_with_result
	connection.should_close_with_result = False
	try:
		# CLog.log("Gonna check of connection liveness")
		connection.scalar(select([1]))
	except exc.DBAPIError as err:
		if err.connection_invalidated:
			CLog.log("Connection failure, gonna try to recreate connection")
			connection.scalar(select([1]))
		else:
			raise
	finally:
		connection.should_close_with_result = save_should_close_with_result


@event.listens_for(engine, "connect")
def connect(dbapi_connection, connection_record):
    connection_record.info['pid'] = os.getpid()
    CLog.log("Connection PID: %s" % connection_record.info['pid'])


@event.listens_for(engine, "checkout")
def checkout(dbapi_connection, connection_record, connection_proxy):
	# CLog.log("Checkout connection for PID: %s" % connection_record.info['pid'])
	pid = os.getpid()
	if connection_record.info['pid'] != pid:
		connection_record.connection = connection_proxy.connection = None
		raise exc.DisconnectionError(
				"Connection record belongs to pid %s, "
				"attempting to check out in pid %s" %
				(connection_record.info['pid'], pid)
		)


def init_v1_routes(api):
    CLog.log("Initiating V1 routes")
    health_checker = HealthCheckerResource()
    yaml_validator = YamlValidatorResource()
    saml_auth = SamlAuthResource()
    auth_token_verification = TokenVerificationResources()
    refresh_token = RefreshAuthToken()
    app = AppResource()
    envs = EnvsResource()
    service_configs = ServiceConfigsResource()
    service_deployer = ServiceDeployerResource()
    env_services_deployments = EnvironmentServicesDeploymentResource()
    env_chart_deployments = EnvironmentChartDeploymentResource()
    service_dockers = ServiceDockersResource()
    # service_k8s_secrets = ServiceSecretResource()
    rollback_deployment = DeploymentRollbackResource()
    pods = PodsResource()
    pod = PodResource()
    pod_log = PodLogResource()
    pod_exec = PodExecResource()
    replicaset = ReplicaSetResource()
    secret = SecretsResource()
    job = JobsResource()
    k8services = K8SServicesResource()
    l5d_links = L5DLinksResource()
    chart_repo = ChartRepoResource()
    chart_deployer = ChartDeployerResource()
    k8s_nodes = K8SNodesListResource()
    k8s_component_status = K8SComponentsStatusResource()
    k8s_resources = K8SResStatusResources()
    deployed_versions = DeployedVersionsResource()
    top_deployed_services = TopDeployedServicesResource()

    # Health check route
    api.add_route('/v1/health', health_checker)

    # SAML2 authentication
    api.add_route('/v1/auth/saml/{mode}', saml_auth)
    api.add_route('/v1/token/{token}', auth_token_verification)
    api.add_route('/v1/token/refresh', refresh_token)

    # Validators
    api.add_route('/v1/env/{env}/validate/depfile', yaml_validator)

    # Environments routes
    api.add_route('/v1/env', envs)
    api.add_route('/v1/env/{env}', envs)

    # Application routes
    api.add_route('/v1/env/application/app', app)
    api.add_route('/v1/env/application/app/{app_name}', app)

    api.add_route('/v1/env/{env}/pods', pods)
    api.add_route('/v1/env/{env}/pods/{did}', pods)
    api.add_route('/v1/env/{env}/pod/{pod_name}', pod)
    api.add_route('/v1/env/{env}/pod/{pod_name}/log', pod_log)
    api.add_route('/v1/env/{env}/pod/{pod_name}/exec', pod_exec)

    api.add_route('/v1/env/{env}/replicaset/{did}', replicaset)
    api.add_route('/v1/env/{env}/job/{did}', job)
    api.add_route('/v1/env/{env}/secret/{did}', secret)
    api.add_route('/v1/env/{env}/k8service/{did}', k8services)
    api.add_route('/v1/env/{env}/k8service/{did}/l5dlinks', l5d_links)

    api.add_route('/v1/env/{env}/deployments/services/versions', deployed_versions)
    api.add_route('/v1/env/{env}/deployments/services/top', top_deployed_services)
    api.add_route('/v1/env/{env}/deployments/services', env_services_deployments)
    api.add_route('/v1/env/{env}/deployments/services/{stats}', env_services_deployments)
    api.add_route('/v1/env/{env}/deployments/charts', env_chart_deployments)
    api.add_route('/v1/env/{env}/deployments/charts/{stats}', env_chart_deployments)

    api.add_route('/v1/env/{env}/chart/deployment', chart_deployer)
    api.add_route('/v1/env/{env}/chart/deployment/{did}', chart_deployer)
    api.add_route('/v1/env/{env}/service/deployment', service_deployer)
    api.add_route('/v1/env/{env}/service/deployment/rollback/{did}', rollback_deployment)
    api.add_route('/v1/env/{env}/service/deployment/{did}', service_deployer)

    # api.add_route('/v1/env/{env}/service/secret/{did}', service_k8s_secrets)
    api.add_route('/v1/env/{env}/service/{service}/configs', service_configs)
    api.add_route('/v1/env/{env}/service/{service}/configs/{role}', service_configs)
    api.add_route('/v1/env/{env}/service/{service}/docker/{registry}', service_dockers)

    api.add_route('/v1/env/{env}/k8s/nodes', k8s_nodes)
    api.add_route('/v1/env/{env}/k8s/components', k8s_component_status)
    api.add_route('/v1/env/{env}/k8s/resources', k8s_resources)

    api.add_route('/v1/env/{env}/repo/{repo}/charts', chart_repo)
    api.add_route('/v1/env/{env}/repo/{repo}/chart/{chart_name}/versions', chart_repo)


def init_v2_routes(api):
    CLog.log("Initiating V2 routes")
    health_checker = HealthCheckerResource()
    yaml_validator = YamlValidatorResource()
    saml_auth = SamlAuthResource()
    auth_token_verification = TokenVerificationResources()
    refresh_token = RefreshAuthToken()
    app = AppResource()
    envs = EnvsResource()
    service_annotations = ServiceAnnotationsResource()
    service_configs = ServiceConfigsResource()
    service_deployer = ServiceDeployerResource()
    env_services_deployments = EnvironmentServicesDeploymentResource()
    env_chart_deployments = EnvironmentChartDeploymentResource()
    service_dockers = ServiceDockersResource()
    # service_k8s_secrets = ServiceSecretResource()
    rollback_deployment = DeploymentRollbackResource()
    pods = PodsResource()
    pod = PodResource()
    pod_log = PodLogResource()
    pod_exec = PodExecResource()
    replicaset = ReplicaSetResource()
    secret = SecretsResource()
    job = JobsResource()
    k8services = K8SServicesResource()
    l5d_links = L5DLinksResource()
    chart_repo = ChartRepoResource()
    chart_deployer = ChartDeployerResource()
    k8s_nodes = K8SNodesListResource()
    k8s_component_status = K8SComponentsStatusResource()
    k8s_resources = K8SResStatusResources()
    deployed_versions = DeployedVersionsResource()
    top_deployed_services = TopDeployedServicesResource()

    # Health check route
    api.add_route('/v2/scope/public/health', health_checker)

    # SAML2 authentication
    api.add_route('/v2/scope/public/auth/saml/{mode}', saml_auth)
    api.add_route('/v2/scope/public/token/{token}', auth_token_verification)
    api.add_route('/v2/scope/public/token/refresh', refresh_token)

    # Validators
    api.add_route('/v2/scope/common/validate/depfile', yaml_validator)

    # Environments routes
    api.add_route('/v2/scope/environment/env', envs)
    api.add_route('/v2/scope/{env}', envs)

    # Application routes
    api.add_route('/v2/scope/application/app', app)
    api.add_route('/v2/scope/application/app/{app_name}', app)

    api.add_route('/v2/scope/{env}/pods', pods)
    api.add_route('/v2/scope/{env}/pods/{did}', pods)
    api.add_route('/v2/scope/{env}/pod/{pod_name}', pod)
    api.add_route('/v2/scope/{env}/pod/{pod_name}/log', pod_log)
    api.add_route('/v2/scope/{env}/pod/{pod_name}/exec', pod_exec)

    api.add_route('/v2/scope/{env}/replicaset/{did}', replicaset)
    api.add_route('/v2/scope/{env}/job/{did}', job)
    api.add_route('/v2/scope/{env}/secret/{did}', secret)
    api.add_route('/v2/scope/{env}/k8service/{did}', k8services)
    api.add_route('/v2/scope/{env}/k8service/{did}/l5dlinks', l5d_links)

    api.add_route('/v2/scope/{env}/deployments/services/versions', deployed_versions)
    api.add_route('/v2/scope/{env}/deployments/services/top', top_deployed_services)
    api.add_route('/v2/scope/{env}/deployments/services', env_services_deployments)
    api.add_route('/v2/scope/{env}/deployments/services/{stats}', env_services_deployments)
    api.add_route('/v2/scope/{env}/deployments/charts', env_chart_deployments)
    api.add_route('/v2/scope/{env}/deployments/charts/{stats}', env_chart_deployments)

    api.add_route('/v2/scope/{env}/chart/deployment', chart_deployer)
    api.add_route('/v2/scope/{env}/chart/deployment/{did}', chart_deployer)
    api.add_route('/v2/scope/{env}/service/deployment', service_deployer)
    api.add_route('/v2/scope/{env}/service/deployment/rollback/{did}', rollback_deployment)
    api.add_route('/v2/scope/{env}/service/deployment/{did}', service_deployer)
        
    api.add_route('/v2/scope/{env}/service/{service}/annotations', service_annotations)
    api.add_route('/v2/scope/{env}/service/{service}/annotations/{role}', service_annotations)

    api.add_route('/v2/scope/{env}/service/{service}/configs', service_configs)
    api.add_route('/v2/scope/{env}/service/{service}/configs/{role}', service_configs)
    api.add_route('/v2/scope/{env}/service/{service}/docker/{registry}', service_dockers)

    api.add_route('/v2/scope/{env}/k8s/nodes', k8s_nodes)
    api.add_route('/v2/scope/{env}/k8s/components', k8s_component_status)
    api.add_route('/v2/scope/{env}/k8s/resources', k8s_resources)

    api.add_route('/v2/scope/{env}/repo/{repo}/charts', chart_repo)
    api.add_route('/v2/scope/{env}/repo/{repo}/chart/{chart_name}/versions', chart_repo)


def init_saml2_metadata():
    """
    Initializing Baiko SAML2 metadata configs from DB
    :return: None
    """
    CLog.log("Initializing Baiko SAML2 metadata configs from DB")
    conf.SAML_METADATA_UI = DPBaikoConfigs().get_baiko_configs_by_type('ui')
    conf.SAML_METADATA_API = DPBaikoConfigs().get_baiko_configs_by_type('api')


def init_kubeconfig():
    """
    create single merged kubeconfig from all available envs in mongoDB
    :return: None
    """
    CLog.log("Initiating kubeconfig for supported K8S environments")
    # db = IG.db_client[conf.DB_NAME]
    envs = DPEnv().get_all_envs()
    # In case where there is not envs found, probably because of a fresh Baiko instance,
    # do not create a kubeconfig file
    if len(envs) == 0:
        CLog.log(
            "There is no K8S environments configured, to be able to use Baiko Servier, add at least one K8S env and restart the server")
        return

    merged_kubeconfig = {
        'apiVersion':      'v1',
        'kind':            'Config',
        'current-context': None,
        'clusters':        [],
        'contexts':        [],
        'users':           []
    }

    for env in envs:
        kubeconfig = yaml.load(str(base64.b64decode(env['kubeconfig']), 'utf-8'))
        for cluster in kubeconfig['clusters']:
            merged_kubeconfig['clusters'].append(cluster)
        for context in kubeconfig['contexts']:
            merged_kubeconfig['contexts'].append(context)
        for user in kubeconfig['users']:
            merged_kubeconfig['users'].append(user)

    merged_kubeconfig['current-context'] = merged_kubeconfig['contexts'][0]['name']

    kubeconfig_file_content = yaml.dump(merged_kubeconfig, default_flow_style=False)
    kubeconfig_file = "%s/%s" % (conf.K8S_CONFIG_DIR, conf.K8S_MERGED_CONF)
    CLog.log("Gonna dump kubeconfig to %s" % kubeconfig_file)
    if not os.path.exists(conf.K8S_CONFIG_DIR):
        os.makedirs(conf.K8S_CONFIG_DIR)
    with open(kubeconfig_file, 'w+') as f:
        f.write(kubeconfig_file_content)
    f.close()
