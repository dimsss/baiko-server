from pyper.resource import PyperBase
from pyper.exception import CException
import falcon
from falcon import HTTPMovedPermanently
from baiko_server.app import bootstrap
from pyper.globals import IG, CG
import datetime
from baiko_server.common import validators
from baiko_server.common.utils import generate_did, DeploymentType, _add_env_suffix
from baiko_server.common import data_provider as datap
from baiko_server.common.api_clients import VaultClient, ArtifactoryClient, KubeClient, KubeCTLClient, HelmRepoClient
from baiko_server.config import conf
from celery import Celery
from pyper.logger import CLog
from pyper.auth import SCOPE
from baiko_server.common.deployment_observer import DeploymentObserver
import kubernetes.client
from kubernetes.client.rest import ApiException
from baiko_server.common.authenticator import OktaSamlAuthenticator, TokenVerifier


class TokenVerificationResources(PyperBase):
    SCOPE = SCOPE.PUBLIC

    def on_get(self, req, resp, token):
        tv = TokenVerifier(token)
        self.payload = tv.get_token_details()


class SamlAuthResource(PyperBase):
    SCOPE = SCOPE.PUBLIC

    def on_post(self, req, resp, mode='api'):
        try:
            auth = OktaSamlAuthenticator(req.context['doc']['SAMLResponse'], mode)
            auth_res = auth.auth()
            if auth_res is False:
                raise Exception()
            token = auth.gen_auth_token()
            if mode == 'ui':
                resp.body = ''
                raise falcon.HTTPMovedPermanently('%s/%s' % (conf.UI_REDIRECT, token))
            else:
                offline_token = auth.get_offline_token()
                if not offline_token:
                    offline_token = auth.gen_offline_auth_token()

                self.payload = {'X-BAIKO-SERVER-AUTH': token, 'X-BAIKO-OFFLINE-TOKEN': offline_token}

        except HTTPMovedPermanently as ex:
            raise ex
        except Exception as ex:
            CLog.log("Unable authenticate, not valid user/pass or any other problem related to Okta/SAML authentication flow")
            raise CException(falcon.HTTP_401, CException.ERROR_STATUS, "Authentication failed")


class RefreshAuthToken(PyperBase):
    SCOPE = SCOPE.PUBLIC

    def on_post(self, req, resp):
        try:
            offline_token = req.context.get('doc').get('X-BAIKO-OFFLINE-TOKEN')
            dpu = datap.DPUser()
            auth = OktaSamlAuthenticator(None, 'api', dpu.get_identity_data_from_offline_token(offline_token))
            new_12h_token = auth.gen_auth_token()
            self.payload = {'X-BAIKO-SERVER-AUTH': new_12h_token}
        except Exception as ex:
            CLog.log("Unable to create new 12h token, given offline token not valid, %s " % ex)
            raise CException(falcon.HTTP_401, CException.ERROR_STATUS, "refresh token failed")


class AuthGroupsResource(PyperBase):
    SCOPE = SCOPE.PRIVATE

    def on_get(self, req, resp, group_name=None):
        db = IG.db_client[conf.DB_NAME].accessGroup
        if group_name:
            self.payload = db.find_one({'accessGroup': group_name}, {'_id': False})

    def on_post(self, req, resp):
        db = IG.db_client[conf.DB_NAME].accessGrops
        access_group = db.find_one({'accessGroup': req.context['doc']['accessGroup']})
        if access_group:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Access group already exists")
        db.insert_one(req.context['doc'])


class UserResource(PyperBase):
    def on_get(self, req, resp):
        pass

    def on_pust(self, req, resp):
        pass


class AppResource(PyperBase):
    def on_get(self, req, resp, app_name=None):
        try:
            dp = datap.DPApp()
            if app_name:
                app = dp.get_app(app_name)
                if app is False:
                    raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "Application not found")
                self.payload = app
            else:
                self.payload = dp.get_apps()
        except Exception as ex:
            raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Error while fetching apps %s" % ex)

    @falcon.before(validators.app_put_post_req_schema)
    def on_post(self, req, resp):
        dp = datap.DPApp()
        if not dp.is_app_exists(req.context['doc']['appName']):
            dp.create_app(req.context['doc'])
            self.status_code = falcon.HTTP_201
        else:
            raise CException(falcon.HTTP_409, CException.ERROR_STATUS, "Application already exists")

    @falcon.before(validators.app_put_post_req_schema)
    def on_put(self, req, resp):
        dp = datap.DPApp()
        if dp.is_app_exists(req.context['doc']['appName']):
            dp.update_app(req.context['doc'])
        else:
            raise CException(falcon.HTTP_401, CException.ERROR_STATUS, "Application not found")

    def on_delete(self, req, resp, app_name):
        datap.DPApp().delete_app(app_name)


class HealthCheckerResource(PyperBase):
    SCOPE = SCOPE.PUBLIC

    def on_get(self, req, resp):
        service_configs = {}
        for c in dir(conf):
            if c.isupper():
                service_configs[c] = getattr(conf, c)
        app = Celery('tasks', backend=conf.REDIS_BACKEND, broker=conf.REDIS_BROKER)
        celery_inspector = app.control.inspect()

        registered_task = celery_inspector.registered()
        ping = celery_inspector.ping()
        celery_conf = celery_inspector.conf()

        celery_ok = True
        if isinstance(ping, dict):
            for celery_host in ping:
                if ping[celery_host]['ok'] != 'pong':
                    celery_ok = False
        else:
            celery_ok = False

        if not celery_ok:
            self.status_code = falcon.HTTP_500
        self.payload = {
            'status': 'up',
            'uptime': str(datetime.datetime.now().replace(microsecond=0) - IG.uptime),
            'service_name': conf.APP_NAME,
            # 'service_configs': service_configs,
            'celery': {
                'celery_ok': celery_ok,
                'ping': ping,
                'registered_task': registered_task,
                "conf": celery_conf
            }
        }


class YamlValidatorResource(PyperBase):
    def on_post(self, req, resp, env):
        docker_registry = req.context['doc']['docker_registry']
        service_name = req.context['doc']['service_name']
        docker_tag = req.context['doc']['docker_tag']
        if validators.validate_depfile(env, docker_registry, service_name, docker_tag):
            self.payload = "Depfiles passed YAML validation"


class EnvsResource(PyperBase):
    """
    The GET method of that resource doesn't require env allowed check
      since route to that method goes like this: /v1/env, as a result the allowed env check 
      should be done inside GET method
    """

    def on_get(self, req, resp, env=None):
        dp = datap.DPEnv()
        if env:
            self.payload = dp.get_env(env)
        else:
            if conf.AUTH_ENABLED:
                access_group = CG.ui.accessGroup
                self.payload = dp.get_envs(access_group)
            else:
                self.payload = dp.get_envs()

    @falcon.before(validators.envs_post_req_schema)
    @falcon.before(validators.k8s_config_req_schema)
    def on_post(self, req, resp):
        dp = datap.DPEnv()
        if not dp.is_env_exists(req.context['doc']['envName']):
            env_context = _add_env_suffix(req.context['doc'])
            validators.validate_env_uniq(env_context)
            dp.create_env(env_context)
            self._reload_kubeconfig()
            self.status_code = falcon.HTTP_201
        else:
            raise CException(falcon.HTTP_409, CException.ERROR_STATUS, "Provided environment already exists")

    @falcon.before(validators.envs_post_req_schema)
    @falcon.before(validators.k8s_config_req_schema)
    def on_put(self, req, resp):
        dp = datap.DPEnv()
        if dp.is_env_exists(req.context['doc']['envName']):
            validators.validate_env_uniq(req.context['doc'])
            dp.update_env(req.context['doc'])
            self._reload_kubeconfig()
        else:
            raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "Provided environment not found")

    def on_delete(self, req, resp, env):
        dp = datap.DPEnv()
        if dp.is_env_exists(env):
            dp.delete_env(env)
            self._reload_kubeconfig()
        else:
            raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "Provided environment not found")

    def _reload_kubeconfig(self):
        bootstrap.init_kubeconfig()
        celery = Celery('tasks', backend=conf.REDIS_BACKEND, broker=conf.REDIS_BROKER)
        celery.send_task('tasks.broadcast_task', args=('tasks.update_config',))


class ServiceAnnotationsResource(PyperBase):
    def on_get(self, req, resp, env, service, role=None):
        vc = VaultClient(env)
        self.payload = vc.fetch_service_annotations(service, role)

    def on_post(self, req, resp, env, service, role=None):
        vc = VaultClient(env)
        vc.push_service_annotation(service=service, data=req.context.get('doc'), role=role)


class ServiceConfigsResource(PyperBase):
    def on_get(self, req, resp, env, service, role=None):
        vc = VaultClient(env)
        self.payload = vc.fetch_service_configs(service, role, secure=True)

    def on_post(self, req, resp, env, service, role=None):
        vc = VaultClient(env)
        vc.push_configs(service, req.context.get('doc'), role)


class EnvironmentServicesDeploymentResource(PyperBase):
    def on_get(self, req, resp, env, stats=False):
        if stats:
            self.payload = datap.DPDObj().get_deployments_stats(env)
        else:
            self.payload = datap.DPDObj().get_deployments(env)


class EnvironmentChartDeploymentResource(PyperBase):
    def on_get(self, req, resp, env, stats=False):
        if stats:
            pass
        else:
            self.payload = datap.DPChartDObj().get_deployments(env)


class ServiceDeployerResource(PyperBase):
    def on_get(self, req, resp, env, did):
        self.payload = datap.DPDObj().get_dobj(did, hidden_conf=True)

    @falcon.before(validators.installer_post_req_schema)
    @falcon.before(validators.check_if_app_exists)
    def on_post(self, req, resp, env):

        # The none in serviceRole is a same as None, in case when service don't have a formal roles,
        # but still should run with different configs,
        # use serviceRole none to keep the original service configs, see Properties service for example
        if req.context['doc']['serviceRole'] == 'none':
            req.context['doc']['serviceRole'] = ''
        drecord = {
            "env_name": req.context['doc']['envName'],
            "service_name": req.context['doc']['serviceName'],
            "docker_registry": req.context['doc']['dockerRegistry'],
            "docker_tag": req.context['doc']['dockerTag'],
            "service_role": req.context['doc']['serviceRole'],
        }

        did = generate_did(DeploymentType.SERVICE)
        celery = Celery('tasks', backend=conf.REDIS_BACKEND, broker=conf.REDIS_BROKER)
        async_res = celery.send_task('tasks.install_service', args=(did, drecord, CG.ui._asdict()))
        self.payload = {'did': did, 'taskId': async_res.id}

    def on_delete(self, req, resp, env, did):
        dobj = datap.DPDObj().get_dobj(did)
        if not dobj:
            raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "DID not found")
        do = DeploymentObserver(did)
        do.delete_deployment()


class ServiceDockersResource(PyperBase):
    def on_get(self, req, resp, env, service, registry):
        dp = datap.DPEnv()
        env = dp.get_env(env)
        if not env:
            raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "Env not found")
        artifactory_url = "%s/artifactory/api" % env['artifactoryBaseUrl']
        ac = ArtifactoryClient(artifactory_url, registry, service)
        res = ac.get_image_tags()
        if 'errors' in res:
            CLog.log("Failed to fetch docker tags from artifactory %s" % res, CLog.ERROR)
            if len(res['errors']) and 'message' in res['errors'][0]:
                raise CException(falcon.HTTP_400, CException.ERROR_STATUS, res['errors'][0]['message'])
            else:
                raise CException(falcon.HTTP_400, CException.ERROR_STATUS, "Unable to retrieve docker tags from Artifactory")

        self.payload = res["tags"]


class ServiceSecretResource(PyperBase):
    def on_get(self, req, resp, did):
        pass


class PodLogResource(PyperBase):
    def on_get(self, req, resp, env, pod_name):
        kc = KubeClient(env)
        resp.body = kc.fetch_pod_logs(pod_name)
        # self.payload = kc.fetch_pod_logs(pod_name)


class PodExecResource(PyperBase):
    def on_post(self, req, res, env, pod_name, namespace="default"):
        kc = KubeClient(env)
        cmd = req.context['doc']['cmd']
        container = None
        if 'container' in req.context['doc']:
            container = req.context['doc']['container']
        self.payload = kc.connect_pod_exec(env, namespace, pod_name, cmd, container)


class PodResource(PyperBase):
    def on_get(self, req, res, env, pod_name):
        kc = KubeClient(env)
        self.payload = kc.fetch_pod(pod_name)

    def on_delete(self, req, res, env, pod_name, namespace="default"):
        kc = KubeClient(env)
        body = kubernetes.client.V1DeleteOptions()
        try:
            response = kc.core_api.delete_namespaced_pod(pod_name, namespace, body)
            self.payload = "Pod %s was deleted" % pod_name
        except ApiException as e:
            raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "Unable to delete pod %s" % e)


class ReplicaSetResource(PyperBase):
    def on_get(self, req, resp, env, did):
        kubectl = KubeCTLClient(did)
        self.payload = kubectl.get_deployment()


class PodsResource(PyperBase):
    def on_get(self, req, res, env, did=None):
        kc = KubeClient(env)
        if did:
            self.payload = kc.fetch_pods(('did', did))
        else:
            self.payload = kc.fetch_pods()


class SecretsResource(PyperBase):
    def on_get(self, req, res, env, did=None):
        kubectl = KubeCTLClient(did)
        self.payload = kubectl.get_secrets()


class JobsResource(PyperBase):
    def on_get(self, req, res, env, did=None):
        kubectl = KubeCTLClient(did)
        self.payload = kubectl.get_jobs()


class K8SServicesResource(PyperBase):
    def on_get(self, req, res, env, did):
        kubectl = KubeCTLClient(did)
        self.payload = kubectl.get_services()


class DeploymentRollbackResource(PyperBase):
    def on_post(self, req, resp, env, did):
        celery = Celery('tasks', backend=conf.REDIS_BACKEND, broker=conf.REDIS_BROKER)
        async_res = celery.send_task('tasks.rollback_service', args=(did,))
        self.payload = {'did': did, 'taskId': async_res.id}


class L5DLinksResource(PyperBase):
    def on_get(self, req, resp, env, did):
        kl = KubeClient(env)
        self.payload = kl.fetch_service(did)


class K8SNodesListResource(PyperBase):
    def on_get(self, req, resp, env):
        cl = KubeClient(env)
        self.payload = cl.get_nodes_list()


class K8SComponentsStatusResource(PyperBase):
    def on_get(self, req, resp, env):
        cl = KubeClient(env)
        self.payload = cl.list_components_status()


class K8SResStatusResources(PyperBase):
    def on_get(self, req, resp, env):
        cl = KubeClient(env)
        self.payload = cl.get_api_resources()


class ChartRepoResource(PyperBase):
    def on_get(self, req, resp, env, repo, chart_name=None):
        if repo and chart_name:
            repo_client = HelmRepoClient(env, repo)
            self.payload = repo_client.list_chart_versions(chart_name)
        else:
            repo_client = HelmRepoClient(env, repo)
            self.payload = repo_client.list_charts()


class ChartDeployerResource(PyperBase):
    def on_get(self, req, resp, env, did):
        data = datap.DPChartDObj().get_dobj(did)
        self.payload = data

    @falcon.before(validators.chart_deploy)
    def on_post(self, req, resp, env):
        irecord = {
            'env_name': env,
            'chart_repo': req.context['doc']['repo'],
            'chart_name': req.context['doc']['chart'],
            'chart_version': req.context['doc']['version']
        }
        did = generate_did(DeploymentType.CHART)
        celery = Celery('tasks', backend=conf.REDIS_BACKEND, broker=conf.REDIS_BROKER)
        async_res = celery.send_task('tasks.install_chart', args=(did, irecord, CG.ui._asdict()))
        self.payload = {'did': did, 'taskId': async_res.id}

    def on_delete(self, req, resp, env, did):
        dobj = datap.DPChartDObj().get_dobj(did)
        if not dobj:
            raise CException(falcon.HTTP_404, CException.ERROR_STATUS, "DID not found")
        do = DeploymentObserver(did)
        do.delete_deployment()


class DeployedVersionsResource(PyperBase):
    def on_get(self, req, resp, env):
        self.payload = []
        kc = KubeClient(env)
        pods = kc.fetch_pods()
        for pod in pods:
            if pod['did'] == '-':
                continue
            self.payload.append({
                'did': pod['did'],
                'appName': pod['app_name'],
                'tsver': pod['tsver']
            })


class TopDeployedServicesResource(PyperBase):
    def on_get(self, req, resp, env):
        self.payload = datap.DPDObj().get_top_deployed_services(env)
