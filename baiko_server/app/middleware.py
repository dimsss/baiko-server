import falcon
from baiko_server.config import conf
from pyper.globals import CG, IG
from pyper.auth import SCOPE
from pyper.exception import CException
from baiko_server.common.data_provider import DPAcl
from pyper.logger import CLog


class DBSessionCleaner(object):
    def process_response(self, req, resp, resource, req_succeeded):
        IG.DBSession.remove()


class CORSMiddleware(object):
    ALLOW_HEADERS = [
        'Access-Control-Allow-Headers',
        'Access-Control-Allow-Headers'
        'Origin',
        'Accept',
        'X-Requested-With',
        'Content-Type',
        "Access-Control-Request-Method",
        "Access-Control-Request-Headers",
        'X-BAIKO-SERVER-AUTH'
    ]
    ALLOW_METHODS = [
        'GET',
        'POST',
        'OPTIONS',
        'PATCH',
        'PUT',
        'DELETE'
    ]

    def process_response(self, req, resp, resource, req_succeeded):
        if req.method == 'OPTIONS':
            cors_header = {
                'Access-Control-Allow-Origin': "%s" % req.env['HTTP_ORIGIN'],
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Methods': ",".join(CORSMiddleware.ALLOW_METHODS),
                'Access-Control-Allow-Headers': ",".join(CORSMiddleware.ALLOW_HEADERS),
                'Content-Type': 'text/plain charset=UTF-8',
                'Content-Length': 0
            }
            resp.set_headers(cors_header)
            raise falcon.HTTPError(falcon.HTTP_OK)

        else:
            cors_header = {
                'Access-Control-Allow-Origin': "%s" % (req.env['HTTP_ORIGIN'] if 'HTTP_ORIGIN' in req.env else '*'),
                'Access-Control-Allow-Credentials': 'true',
            }
            resp.set_headers(cors_header)


class AccessPermissionMiddleware(object):
    def process_resource(self, req, resp, resource, params):
        # If accessing to public resources, continue request without authentication checks
        if SCOPE.PUBLIC == resource.SCOPE:
            return
        # If authentication disabled, do not check permissions
        if not conf.AUTH_ENABLED:
            return
        # check if resource and method allowed for access
        if self._is_access_allowed(CG.ui.email, CG.ui.accessGroup, req, resource):
            return
        else:
            raise CException(falcon.HTTP_405, CException.ERROR_STATUS, "Access to selected env is forbidden")

    def _is_access_allowed(self, user_email, access_group, req, resource):
        try:
            resource = resource.__class__.__name__
            env = self._get_env(req.path)
            dp_acl = DPAcl()
            CLog.log("Gonna validate if user %s with access group %s can access to %s by %s" % (user_email, access_group, resource, req.method.lower()))
            rule = dp_acl.get_acl_rule(user_email, access_group, resource)
            if env:
                # As for now, authorization is done by env only
                # '''
                #  Check if user exists
                #  and
                #  Check if user access groups allowed to access to resource and methods
                #  and
                #  Check if selected env is listed in access group allowed envs
                # '''
                # if rule['user_exists'] is True and rule[req.method.lower()] is True and env in rule['allowed_envs']:
                #     return True

                '''
                Check if user exists
                and
                Check if selected env is listed in access group allowed envs
                '''
                if rule['user_exists'] is True and env in rule['allowed_envs']:
                    return True
            else:
                # As for now, authorization is done by env only
                # '''
                #  Check if user exists and if user access groups allowed to access to resource and methods
                #  The env level check should be implemented inside controller, since the /v1/env routes doesn't include env name parameter
                # '''
                # if rule['user_exists'] is True and rule[req.method.lower()] is True:
                #     return True
                if rule['user_exists'] is True:
                    return True
            return False
        except Exception as ex:
            CLog.log("Failure while checking if access is allowed, access goona be forbidden, ex: %s" % ex, CLog.ERROR)
            return False

    def _get_env(self, path):
        path_list = path.split('/')
        if len(path_list) <= 3:
            return False
        else:
            return path_list[3]
