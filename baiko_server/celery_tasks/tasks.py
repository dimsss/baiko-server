import os
import sys
from celery import Celery
from celery import exceptions

sys.path.append(os.path.join("{0}/{1}".format(os.path.dirname(__file__), '../')))
from baiko_server.config import conf
from baiko_server.common.utils import dobj_update_n_sync
from baiko_server.common.deployment_observer import DeploymentObserver
from sqlalchemy import exc, event, select
from pyper.globals import IG, CG
from pyper.logger import CLog
from pyper.exception import CException
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session
from baiko_server.app import bootstrap
from collections import namedtuple
#
celery = Celery('tasks', backend=conf.REDIS_BACKEND, broker=conf.REDIS_BROKER)
# engine = create_engine('postgresql://%s:%s@%s:%s/%s' % (conf.DB_USER, conf.DB_PASS, conf.DB_HOST, conf.DB_PORT, conf.DB_NAME))
# session_factory = sessionmaker(bind=engine)
# IG.DBSession = scoped_session(session_factory)
#
#
# @event.listens_for(engine, "engine_connect")
# def ping_connection(connection, branch):
#     if branch:
#         return
#     save_should_close_with_result = connection.should_close_with_result
#     connection.should_close_with_result = False
#     try:
#         CLog.log("Gonna check of connection liveness")
#         connection.scalar(select([1]))
#     except exc.DBAPIError as err:
#         if err.connection_invalidated:
#             CLog.log("Connection failure, gonna try to recreate connection")
#             connection.scalar(select([1]))
#         else:
#             raise
#     finally:
#         connection.should_close_with_result = save_should_close_with_result
#
#
# @event.listens_for(engine, "connect")
# def connect(dbapi_connection, connection_record):
#     connection_record.info['pid'] = os.getpid()
#     CLog.log("Connection PID: %s" % connection_record.info['pid'])
#
#
# @event.listens_for(engine, "checkout")
# def checkout(dbapi_connection, connection_record, connection_proxy):
#     CLog.log("Checkout connection for PID: %s" % connection_record.info['pid'])
#     pid = os.getpid()
#     if connection_record.info['pid'] != pid:
#         connection_record.connection = connection_proxy.connection = None
#         raise exc.DisconnectionError(
#                 "Connection record belongs to pid %s, "
#                 "attempting to check out in pid %s" %
#                 (connection_record.info['pid'], pid)
#         )


# Init kubeconfig for all configured envs
bootstrap.init_kubeconfig()

# Close all DB related stuff before forking
IG.DBSession.remove()
bootstrap.engine.dispose()


class BaseTask(celery.Task):
    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        # print("Going remove session: %s" % IG.DBSession())
        IG.DBSession.remove()


@celery.task(base=BaseTask)
def install_service(did, irecord, ui):
    CG.ui = _get_ui(ui)
    try:
        io = DeploymentObserver(did, irecord)
        io.trigger_installation()
        dobj_update_n_sync(did, {'status': 'successfully_installed'})

    except CException as ex:
        dobj_update_n_sync(did, {'status': 'error', 'errorMessage': ex.description})
        raise ex
    except Exception as ex:
        dobj_update_n_sync(did, {'status': 'error', 'errorMessage': str(ex)})
        raise ex


@celery.task(base=BaseTask)
def install_chart(did, irecord, ui):
    CG.ui = _get_ui(ui)
    try:
        io = DeploymentObserver(did, irecord)
        io.trigger_installation()
        dobj_update_n_sync(did, {'status': 'successfully_installed'})

    except CException as ex:
        dobj_update_n_sync(did, {'status': 'error', 'errorMessage': ex.description})
        raise ex
    except Exception as ex:
        dobj_update_n_sync(did, {'status': 'error', 'errorMessage': str(ex)})
        raise ex


@celery.task(base=BaseTask)
def rollback_service(did):
    try:
        dobj_update_n_sync(did, {'status': 'rollback'})
        io = DeploymentObserver(did)
        io.rollback_service()
        dobj_update_n_sync(did, {'status': 'successfully_installed'})
    except Exception as ex:
        dobj_update_n_sync(did, {'status': 'error', 'errorMessage': ex})
        raise ex


@celery.task(base=BaseTask)
def update_config():
    bootstrap.init_kubeconfig()


@celery.task(base=BaseTask)
def broadcast_task(task_name):
    worker_control = celery.control.inspect()
    try:
        worker_control.active_queues().keys()
    except Exception:
        raise CException(exceptions.CeleryError, CException.ERROR_STATUS, "No Celery Queues found. Is Celery running?")

    for worker in worker_control.active_queues():
        try:
            routing_key = worker_control.active_queues()[worker][0]['routing_key']
        except KeyError:
            raise CException(exceptions.CeleryError, CException.ERROR_STATUS, f"No routing key found in {worker}")
        celery.send_task(task_name, queue=routing_key)
        CLog.log("Executed task %s in Queue %s" % (task_name, routing_key))


def _get_ui(ui):
    keys = list(ui.keys())
    UI = namedtuple('UI', keys)
    return UI(**ui)
