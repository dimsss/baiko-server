import datetime
import falcon
from pyper.exception import exception_serializer
from pyper.globals import IG
from pyper.middleware import RequestMiddleware, AuthMiddleware
from baiko_server.config import conf
from baiko_server.app import bootstrap
from baiko_server.app.middleware import CORSMiddleware, AccessPermissionMiddleware, DBSessionCleaner

# Up time counter
IG.uptime = datetime.datetime.now().replace(microsecond=0)
# Init kubeconfig for all configured envs
bootstrap.init_kubeconfig()
# Init Baiko SAML2
bootstrap.init_saml2_metadata()
# Clean connection pool before forking
IG.DBSession.remove()
bootstrap.engine.dispose()
# Create WSGI application instance
api = application = falcon.API(middleware=[
	CORSMiddleware(),
	RequestMiddleware(),
	AuthMiddleware(conf.APP_NAME, conf.JWT_SECRET, conf.AUTH_ENABLED, conf.ANONYMOUS_TOKEN),
	AccessPermissionMiddleware(),
	DBSessionCleaner()
])
falcon.RequestOptions.auto_parse_form_urlencoded = True
# Init V1 application routes
bootstrap.init_v1_routes(api)
# Init V2 application routes
bootstrap.init_v2_routes(api)
# Add custom serializer for exception handling over REST
api.set_error_serializer(exception_serializer)
