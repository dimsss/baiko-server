#!/usr/bin/env bash
docker rmi -f docker-dev-local.artifactory-dev.traiana.com/baiko_ui
docker rmi -f docker-dev-local.artifactory-dev.traiana.com/baiko_server
docker rmi -f docker-dev-local.artifactory-dev.traiana.com/baiko_celery
docker rmi -f docker-dev-local.artifactory-dev.traiana.com/baiko_slack
/usr/bin/docker-compose down
/usr/bin/docker-compose up -d
/usr/bin/docker-compose ps