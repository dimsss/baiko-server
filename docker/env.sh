#!/usr/bin/env bash
# Baiko UI
BAIKO_SERVER_EXTERNAL_URL=10.100.8.63:81
AUTH_UI_REDIRECT=http://10.126.4.5/login
OKTA_LOGIN_LINK=https://nex-internal.okta-emea.com/home/nexgroup_baikoui_1/0oa1jak8zukUC2axT0i7/aln1jatn0hWEHE5AV0i7
AUTH_ENABLED=enabled

# Baiko Server
REDIS_IP=redis
DB_HOST=baikolab.c2uwyzkmhbsy.eu-west-1.rds.amazonaws.com
DB_NAME=baiko
DB_USER=baiko_lab
DB_PASS=baiko_master
L5D_HOOK_ENABLED=enabled

# Baiko Slack
BAIKO_ENV=none
BAIKO_SERVER=http://10.100.8.63:81
V_TOKEN=verification_token